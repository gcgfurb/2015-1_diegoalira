package com.opengl.example.opengl;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Diego on 03/03/2015.
 */
public class Painter extends SurfaceView implements Runnable {

    private SurfaceHolder holder;
    private Paint paint;
    private Thread renderThread = null;
    private int xTouch = -1;
    private int yTouch = -1;

    public Painter(Context context) {
        super(context);

        this.holder = getHolder();
        this.paint =  new Paint();
    }

    @Override
    public void run() {
        while(true) {

            if (!holder.getSurface().isValid()) {
                continue;
            }

            Canvas canvas = holder.lockCanvas();

            canvas.drawColor(Color.BLACK);

            paint.setARGB(255, 255, 255, 255);
            paint.setTextSize(40f);
            String s = "Use os dedos";
            canvas.drawText(s, (getWidth()/2)-(paint.measureText(s)/2), 40, paint);

            canvas.drawCircle(getxTouch()-25, getyTouch()-25, 50, paint);

            holder.unlockCanvasAndPost(canvas);
        }
    }

    public void resume() {
        renderThread = new Thread(this);
        renderThread.start();
    }

    public int getxTouch() {
        return xTouch;
    }

    public void setxTouch(int xTouch) {
        this.xTouch = xTouch;
    }

    public int getyTouch() {
        return yTouch;
    }

    public void setyTouch(int yTouch) {
        this.yTouch = yTouch;
    }
}
