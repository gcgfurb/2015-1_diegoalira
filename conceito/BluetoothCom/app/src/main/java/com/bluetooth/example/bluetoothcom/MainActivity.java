package com.bluetooth.example.bluetoothcom;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bluetooth.example.bluetoothcom.event.DiscoveredDeviceEvent;
import com.bluetooth.example.bluetoothcom.exception.BluetoothNotSupportedException;
import com.bluetooth.example.bluetoothcom.exception.OperationCanceledException;
import com.bluetooth.example.bluetoothcom.listener.DiscoveredDeviceListener;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ligar(View v) {
        try {
            Bluetooth.getInstance().enableBluetooth(this);
        } catch (BluetoothNotSupportedException e) {
            Toast.makeText(this, "Este dispositivo não suporta BlueTooth", Toast.LENGTH_SHORT);
        } catch (OperationCanceledException e) {
            Toast.makeText(this, "O usuário negou a apermissão", Toast.LENGTH_SHORT);
        }
    }

    public void tornarVisivel(View v) {
        try {
            Bluetooth.getInstance().initDiscoverability(this);
        } catch (OperationCanceledException e) {
            Toast.makeText(this, "O usuário negou a apermissão", Toast.LENGTH_SHORT);
        }
    }

    public void procurar(View v) {
        Bluetooth.getInstance().startDeviceDiscovery(new DiscoveredDeviceListener() {
            @Override
            public void deviceFound(DiscoveredDeviceEvent event) {
               Toast.makeText(getApplicationContext(), "Dispositivo encontrado " + event.getDevice().getName(), Toast.LENGTH_SHORT).show();
            }
        }, this);
    }

    public void paired(View v) {
        for (BluetoothDevice bluetoothDevice : Bluetooth.getInstance().getPairedDevices()) {
            Toast.makeText(getApplicationContext(), "Dispositivo " + bluetoothDevice.getName(), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
