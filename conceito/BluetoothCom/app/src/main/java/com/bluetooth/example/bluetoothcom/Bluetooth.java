package com.bluetooth.example.bluetoothcom;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.bluetooth.example.bluetoothcom.event.DiscoveredDeviceEvent;
import com.bluetooth.example.bluetoothcom.exception.BluetoothNotSupportedException;
import com.bluetooth.example.bluetoothcom.exception.OperationCanceledException;
import com.bluetooth.example.bluetoothcom.listener.DiscoveredDeviceListener;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Diego on 28/02/2015.
 */
public final class Bluetooth {

    private BluetoothAdapter mBluetoothAdapter;
    private BroadcastReceiver mReceiver;

    private static Bluetooth self;

    private Bluetooth(){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public static Bluetooth getInstance() {
        if (self == null) {
            self = new Bluetooth();
        }
        return self;
    }

    /**
     * Obtem se o dispositivo possui suporte a bluettoh;
     * @return Se o dispositivo possui suporte a bluettoh;
     */
    public boolean isBluetoothSupported() {
        if (mBluetoothAdapter == null) {
            return false;
        }
        return true;
    }

    /**
     * Obtem se o bluetooth do dispositivo esta habilitado.
     * @return Se o bluetooth do dispositivo esta habilitado.
     * @throws BluetoothNotSupportedException Caso o dispositivo não possua bluetooth.
     */
    public boolean isBluetoothEnabled() throws BluetoothNotSupportedException {
        if (!isBluetoothSupported()) {
            throw new BluetoothNotSupportedException();
        }

        return mBluetoothAdapter.isEnabled();
    }

    /**
     *
     * @param activity Atividade atual. Será usada para apresentar ao usuário a opção </br>
     *                 de habilitação do bluetooth.
     * @throws BluetoothNotSupportedException Caso o dispositivo não possua bluetooth.
     * @throws OperationCanceledException Caso o usuário negue a ativação do bluetooth.
     */
    public void enableBluetooth(Activity activity) throws BluetoothNotSupportedException, OperationCanceledException {
        if (!isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            int requestActivationStatus = -1;
            activity.startActivityForResult(enableBtIntent, requestActivationStatus);
        }
    }

    //TODO: Criar método listener para a mudança de estado do dispositivo bluetooth.

    //TODO: Criar método que obtem lista de dispositivos já paerados (histórico).

    /*
      * TODO: Cruar método que verifica se o programa tem as permissões adequadas.
      * http://stackoverflow.com/questions/7203668/how-permission-can-be-checked-at-runtime-without-throwing-securityexception
     */


    /**
     * Registra um listener {@link DiscoveredDeviceListener}. Este listener será </br>
     * notificado quando novos dispoditivos forem encontrados.
     *
     * @param listener Listener que será notificado quando novos dispositivos forem encontrados.
     * @param activity Atividade atual.
     */
    public void startDeviceDiscovery(final DiscoveredDeviceListener listener, Activity activity) {
        if (mReceiver != null) {
            stopDeviceDiscovery(activity);
        }
        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    listener.deviceFound(new DiscoveredDeviceEvent() {
                        @Override
                        public com.bluetooth.example.bluetoothcom.BluetoothDevice getDevice() {
                            return new com.bluetooth.example.bluetoothcom.BluetoothDevice(device.getName(), device.getAddress());
                        }
                    });
                }
            }
        };

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_UUID);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        activity.registerReceiver(mReceiver, filter);

        mBluetoothAdapter.startDiscovery();
    }

    /**
     * Finaliza a busca por novos dispositivos, iniciada com o method {@link #startDeviceDiscovery(com.bluetooth.example.bluetoothcom.listener.DiscoveredDeviceListener, android.app.Activity) startDeviceDiscovery}
     * @param activity
     */
    public void stopDeviceDiscovery(Activity activity) {
        if (mReceiver == null) {
            throw new IllegalStateException("O dispositivo não esta em modo de busca.");
        }
        mBluetoothAdapter.cancelDiscovery();
        activity.unregisterReceiver(mReceiver);
        mReceiver = null;
    }

    /**
     * Permite que o dispositivo seja visivel por um periodo de 5 minutos.
     * @param activity Activity atual;
     */
    public void initDiscoverability(Activity activity) throws OperationCanceledException {
        Intent discoverableIntent = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        int result = -1;
        activity.startActivityForResult(discoverableIntent, result);
    }

    public Set<com.bluetooth.example.bluetoothcom.BluetoothDevice> getPairedDevices() {
        Set<com.bluetooth.example.bluetoothcom.BluetoothDevice> retorno = new HashSet<>();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                retorno.add(new com.bluetooth.example.bluetoothcom.BluetoothDevice(device.getName(), device.getAddress()));
            }
        }

        return retorno;
    }
}