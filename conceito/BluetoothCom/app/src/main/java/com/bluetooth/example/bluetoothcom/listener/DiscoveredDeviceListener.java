package com.bluetooth.example.bluetoothcom.listener;

import com.bluetooth.example.bluetoothcom.event.DiscoveredDeviceEvent;

/**
 * Created by Diego on 01/03/2015.
 */
public interface DiscoveredDeviceListener {

    void deviceFound(DiscoveredDeviceEvent event);

}
