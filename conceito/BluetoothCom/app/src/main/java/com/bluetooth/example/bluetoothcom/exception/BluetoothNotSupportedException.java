package com.bluetooth.example.bluetoothcom.exception;

/**
 * Created by Diego on 28/02/2015.
 */
public class BluetoothNotSupportedException extends Exception {

    public BluetoothNotSupportedException() {
        super("Este dispositivo não suporta bluetooth");
    }
}
