package com.bluetooth.example.bluetoothcom.event;

import com.bluetooth.example.bluetoothcom.BluetoothDevice;

/**
 * Created by Diego on 28/02/2015.
 */
public interface DiscoveredDeviceEvent {

    BluetoothDevice getDevice();

}
