package com.bluetooth.example.bluetoothcom.exception;

/**
 * Created by Diego on 28/02/2015.
 */
public class OperationCanceledException extends Exception {

    public OperationCanceledException(String detailMessage) {
        super(detailMessage);
    }

}
