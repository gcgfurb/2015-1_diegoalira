package com.bluetooth.example.bluetoothcom;

/**
 * Created by Diego on 02/03/2015.
 */
public class BluetoothDevice {

    private String name;
    private String address;

    public BluetoothDevice(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
}
