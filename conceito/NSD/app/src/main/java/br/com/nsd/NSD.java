package br.com.nsd;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Diego on 13/03/2015.
 */
public class NSD {

    private static final String TAG = NSD.class.getName();
    private static final String SERVICE_TYPE = "_http._tcp.";

    //Verificar para colocar este em um arquivo XML de configuração do app usuário
    private static final String SERVICE_NAME = "test_dns";

    private NsdManager nsdManager = null;
    private NsdManager.RegistrationListener registrationListener = null;
    private NsdManager.DiscoveryListener mDiscoveryListener = null;
    private NsdManager.ResolveListener mResolveListener = null;
    private NsdServiceInfo externalServiceInfo = null;

    private Thread serverThread = null;
    private ServerSocket serverSocket = null;
    private Socket clientSocket = null;

    private static NSD instance = null;

    private NSD(Context context) throws IOException {
        this.nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);

        initializeDiscoveryListener();
        initializeRegistrationListener();
        initializeResolveListener();
    }

    private void initClientSocket() throws IOException {
        if (clientSocket == null) {
            clientSocket = new Socket(externalServiceInfo.getHost(), externalServiceInfo.getPort());
        }
    }

    public static NSD getInstance(Context context) {
        if (instance == null) {
            try {
                instance = new NSD(context);
            }catch(Exception ex) {
                throw new IllegalStateException(ex);
            }
        }
        return instance;
    }

    public void registerService() throws IOException {
        this.registerService(getNextAvaliablePortSocket());
    }

    public void sendMessage(String message) throws IOException {
        initClientSocket();

        PrintWriter out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(clientSocket.getOutputStream())),
                true);
        out.println(message);
    }

    public void startDiscovery() {
        nsdManager.discoverServices(
                SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener);
    }

    public void registerService(int port) {
        NsdServiceInfo serviceInfo  = new NsdServiceInfo();

        serviceInfo.setServiceName(SERVICE_NAME);
        serviceInfo.setServiceType(SERVICE_TYPE);
        serviceInfo.setPort(port);

        //
        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();

        nsdManager.registerService(
                serviceInfo, NsdManager.PROTOCOL_DNS_SD, this.registrationListener);
    }

    private int getNextAvaliablePortSocket() throws IOException {
        // Initialize a server socket on the next available port.
        ServerSocket mServerSocket = new ServerSocket(0);

        // Get the chosen port.
        return mServerSocket.getLocalPort();
    }

    private void initializeRegistrationListener() {
        this.registrationListener = new NsdManager.RegistrationListener() {

            @Override
            public void onServiceRegistered(NsdServiceInfo NsdServiceInfo) {
                // Save the service name.  Android may have changed it in order to
                // resolve a conflict, so update the name you initially requested
                // with the name Android actually used.
                String mServiceName = NsdServiceInfo.getServiceName();
            }

            @Override
            public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Registration failed!  Put debugging code here to determine why.
            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo arg0) {
                // Service has been unregistered.  This only happens when you call
                // NsdManager.unregisterService() and pass in this listener.
            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Unregistration failed.  Put debugging code here to determine why.
            }
        };
    }

    private void initializeDiscoveryListener() {
        // Instantiate a new DiscoveryListener
        this.mDiscoveryListener = new NsdManager.DiscoveryListener() {

            //  Called as soon as service discovery begins.
            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found!  Do something with it.
                Log.d(TAG, "Service discovery success" + service);
                if (!service.getServiceType().equals(SERVICE_TYPE)) {
                    // Service type is the string containing the protocol and
                    // transport layer for this service.
                    Log.d(TAG, "Unknown Service Type: " + service.getServiceType());
                } else if (service.getServiceName().contains(SERVICE_NAME)){
                    nsdManager.resolveService(service, mResolveListener);
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                Log.e(TAG, "service lost" + service);
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }
        };
    }

    private void initializeResolveListener() {
        mResolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Called when the resolve fails.  Use the error code to debug.
                Log.e(TAG, "Resolve failed" + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.e(TAG, "Resolve Succeeded. " + serviceInfo);

                if (serviceInfo.getServiceName().equals(SERVICE_NAME)) {
                    Log.d(TAG, "Same IP.");
                    return;
                }
                externalServiceInfo = serviceInfo;
            }
        };
    }

    private class ServerThread implements Runnable {

        public void run() {
            Socket socket = null;

            try {
                serverSocket = new ServerSocket(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (!Thread.currentThread().isInterrupted()) {

                try {

                    socket = serverSocket.accept();

                    CommunicationThread commThread = new CommunicationThread(socket);
                    new Thread(commThread).start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;

            try {

                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {

            while (!Thread.currentThread().isInterrupted()) {

                try {

                    String read = input.readLine();

                    //TODO: Notificar recebimento de comm

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /*
        TODO: Tratar tearDown no pause, resume e destroy.
        Teria que ser consistente com a activity ed forma a aproveitar os métodos onresumo, onpause e ondestroy
     */

}
