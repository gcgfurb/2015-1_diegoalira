package br.com.palavras.mesh;

import android.opengl.GLES20;

import java.nio.FloatBuffer;

import br.com.furbralibrary.utils.render.Texture;
import br.com.furbralibrary.utils.render.RenderProgram;
import br.com.furbralibrary.utils.render.object.MeshObject;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.GLES20.glGetUniformLocation;

/**
 * Created by Diego on 26/04/2015.
 */
public class TexturedObject extends MeshObject {

    private static final int TEXTURE_COMPONENT_COUNT = 2;
    private static final int POSITION_COMPONENT_COUNT = 2;
    private static final String POSITION_ATTRIBUTE_NAME = "a_Position";
    private static final String POSITION_MATRIX_NAME = "u_Matrix";

    public static final String VERTEX_SHADER = " \n" + "\n" +
            "uniform mat4 u_Matrix;\n" +
            "attribute vec4 a_Position;\n" +
            "attribute vec2 a_TextureCoordinates;\n" +
            "varying vec2 v_TextureCoordinates;\n" +
            "void main()\n" +
            "{\n" +
            "v_TextureCoordinates = a_TextureCoordinates;\n" +
            "gl_Position = u_Matrix * a_Position;\n" +
            "}";

    public static final String FRAGMENT_SHADER = " \n" + "\n"
            + "precision mediump float; \n"
            + " \n"
            + "uniform sampler2D u_TextureUnit; \n"
            + " \n"
            + "varying vec2 v_TextureCoordinates; \n"
            + " \n"
            + "void main() \n"
            + "{ \n"
            + "   gl_FragColor = texture2D(u_TextureUnit, v_TextureCoordinates); \n"
            + "} \n";

    private static final float[] vertices = {
            0, 0,
            -1f, -1f,
            1f, -1f,
            1f, 1f,
            -1f, 1f
    };

    private static final short[] indices = {
            0, 1, 2, 3, 4, 1
    };

    private static final float cubeTexcoords[] = {
            0.5f, 0.5f,
            0, 0,
            1, 0,
            1, 1,
            0, 1
    };

    private int vertexTexLocation = 0;
    private int textureUnitLocation = 0;

    private FloatBuffer texBuffer;
    private Texture texture;

    public TexturedObject(Texture texture) {
        super(vertices, indices, new RenderProgram(VERTEX_SHADER, FRAGMENT_SHADER));
        this.texture = texture;
        this.vertexTexLocation = glGetAttribLocation(renderProgram.getProgramLocation(), "a_TextureCoordinates");
        this.textureUnitLocation = glGetUniformLocation(renderProgram.getProgramLocation(), "u_TextureUnit");

        this.texBuffer = createBuffer(cubeTexcoords);

        this.texture.bindTexture();
    }

    public void draw(float[] matrix) {
        glUseProgram(renderProgram.getProgramLocation());

        texBuffer.position(0);
        glVertexAttribPointer(vertexTexLocation, TEXTURE_COMPONENT_COUNT, GL_FLOAT, false, 0, texBuffer);
        glEnableVertexAttribArray(vertexTexLocation);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                texture.getTextureID()[0]);

        GLES20.glUniform1i(textureUnitLocation, 0);

        super.draw(matrix);
    }

    @Override
    protected int getNumPositionCoordsToVertice() {
        return POSITION_COMPONENT_COUNT;
    }

    @Override
    protected String getPositionAttribName() {
        return POSITION_ATTRIBUTE_NAME;
    }

    @Override
    protected String getPositionMatrixUniformName() {
        return POSITION_MATRIX_NAME;
    }

}
