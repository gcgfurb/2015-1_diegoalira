package br.com.palavras.comm;

import br.com.furbralibrary.communication.Message;

/**
 * Created by Diego on 09/05/2015.
 */
public class UserGameInviteReplyMessage extends Message {

    public static final int NO = 1;
    public static final int YES = 0;

    private int reply;

    public UserGameInviteReplyMessage(int reply) {
        this.reply = reply;
    }

    public int getReply() {
        return reply;
    }
}
