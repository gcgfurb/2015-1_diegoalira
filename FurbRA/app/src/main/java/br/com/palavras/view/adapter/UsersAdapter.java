package br.com.palavras.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import br.com.furbra.R;
import br.com.furbralibrary.communication.LanService;

/**
 * Created by Diego on 09/05/2015.
 */
public class UsersAdapter extends BaseAdapter {

    private static int[] imagens = new int[]{
            R.drawable.cachorro,
            R.drawable.gato,
            R.drawable.coelho,
            R.drawable.macaco,
            R.drawable.zebra
    };
    private Context context;
    private Map<LanService, String> serviceNameMap;
    private Map<LanService, Integer> userImageMap;

    public UsersAdapter(Context context, Map<LanService, String> serviceNameMap) {
        this.serviceNameMap = serviceNameMap;
        this.context = context;
                prepareImageMap();
    }

    @Override
    public int getCount() {
        return serviceNameMap.size();
    }

    @Override
    public LanService getItem(int position) {
        Set<LanService> lanServices = serviceNameMap.keySet();
        if (position >= lanServices.size()) {
            return null;
        }
        return new ArrayList<>(lanServices).get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();

        prepareImageMap();
    }

    private void prepareImageMap() {
        this.userImageMap = new HashMap<>();
        for (int i = 0;i < getCount(); i++) {
            userImageMap.put(getItem(i), imagens[i % imagens.length]);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LanService lanService = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false);
        }

        ImageView ivImagem = (ImageView) convertView.findViewById(R.id.imagemIV);
        TextView tvNome = (TextView) convertView.findViewById(R.id.nomeTV);

        ivImagem.setImageResource(userImageMap.get(lanService));
        tvNome.setText(serviceNameMap.get(lanService));

        return convertView;
    }
}
