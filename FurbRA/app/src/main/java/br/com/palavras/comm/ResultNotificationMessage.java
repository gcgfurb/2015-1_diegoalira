package br.com.palavras.comm;

import br.com.furbralibrary.communication.Message;

/**
 * Created by Diego on 13/05/2015.
 */
public class ResultNotificationMessage extends Message {

    private boolean acertou;
    private String palavra;

    public ResultNotificationMessage(boolean acertou, String palavra) {
        this.acertou = acertou;
        this.palavra = palavra;
    }

    public boolean isAcertou() {
        return acertou;
    }

    public String getPalavra() {
        return palavra;
    }
}
