package br.com.palavras.model;

import java.io.Serializable;

import br.com.furbralibrary.communication.LanService;

/**
 * Created by Diego on 13/05/2015.
 */
public class Player implements Serializable {

    private LanService lanService;
    private String userName;

    public Player(LanService lanService, String userName) {
        this.lanService = lanService;
        this.userName = userName;
    }

    public LanService getLanService() {
        return lanService;
    }

    public void setLanService(LanService lanService) {
        this.lanService = lanService;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
