package br.com.palavras.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import br.com.furbra.R;

public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_menu);
    }

    public void aboutClick(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void startGame(View view) {
        Intent intent = null;
        if (view.getId() == R.id.multiBtn) {
            intent = new Intent(this, UsersListActivity.class);
        }else if (view.getId() == R.id.sozinhoBtn) {
            intent = new Intent(this, SingleGameActivity.class);
        }
        startActivity(intent);
    }

}
