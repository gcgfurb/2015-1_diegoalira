package br.com.palavras.comm;

import br.com.furbralibrary.communication.Message;

/**
 * Created by Diego on 10/05/2015.
 */
public class UserNameSolicitationReplyMessage extends Message {

    private String name;

    public UserNameSolicitationReplyMessage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
