package br.com.palavras.mesh;

import br.com.furbralibrary.utils.render.RenderProgram;
import br.com.furbralibrary.utils.render.object.MeshObject;

import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform4fv;
import static android.opengl.GLES20.glUseProgram;

/**
 * Created by Diego on 26/04/2015.
 */
public class CubeObject extends MeshObject {

    private static final int POSITION_COMPONENT_COUNT = 2;
    private static final String POSITION_ATTRIBUTE_NAME = "a_Position";
    private static final String POSITION_MATRIX_NAME = "u_Matrix";

    private static final String VERTEX_SHADER = "" +
            "uniform mat4 u_Matrix; \n" +
            "\n" +
            "attribute vec4 a_Position; \n" +
            "\n" +
            "void main() {\n" +
            "\n" +
            "gl_Position = u_Matrix * a_Position;\n" +
            "\n" +
            "}";

    private static final String FRAGMENT_SHADER = "" +
            "precision mediump float;\n" +
            "\n" +
            "uniform vec4 u_Color; \n" +
            "\n" +
            "void main() {\n" +
            "\n" +
            "gl_FragColor = u_Color;\n" +
            "\n" +
            "}";

    private static final float[] vertices = {
            0, 0,
            -1f, -1f,
            1f, -1f,
            1f, 1f,
            -1f, 1f
    };

    private static final short[] indices = {
            0, 1, 2, 3, 4, 1
    };

    private final int colorLocation;
    private float[] color = new float[]{1f, 1f, 1f, 1f};

    public CubeObject() {
        super(vertices, indices, new RenderProgram(VERTEX_SHADER, FRAGMENT_SHADER));

        this.colorLocation = glGetUniformLocation(this.renderProgram.getProgramLocation(), "u_Color");
    }

    public float[] getColor() {
        return color;
    }

    public void setColor(float[] color) {
        this.color = color;
    }

    public void setColor(float r, float g, float b, float a) {
        this.color = new float[] {r, g, b, a};
    }

    public void draw(float[] matrix) {
        glUseProgram(this.renderProgram.getProgramLocation());
        glUniform4fv(colorLocation, 1, color, 0);
        super.draw(matrix);
    }

    @Override
    protected int getNumPositionCoordsToVertice() {
        return POSITION_COMPONENT_COUNT;
    }

    @Override
    protected String getPositionAttribName() {
        return POSITION_ATTRIBUTE_NAME;
    }

    @Override
    protected String getPositionMatrixUniformName() {
        return POSITION_MATRIX_NAME;
    }

}
