package br.com.palavras.activity;

import android.app.Dialog;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.Vuforia;
import com.qualcomm.vuforia.Word;
import com.qualcomm.vuforia.WordResult;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import br.com.furbra.R;
import br.com.furbralibrary.activity.FurbRaTextRecognitionActivity;
import br.com.furbralibrary.communication.LanService;
import br.com.furbralibrary.communication.LocalNetworkCommunication;
import br.com.furbralibrary.communication.Message;
import br.com.furbralibrary.communication.listener.LocalExternalServiceDiscoveryListener;
import br.com.furbralibrary.communication.listener.LocalNetworkMessageReceivedListener;
import br.com.furbralibrary.media.SoundBox;
import br.com.furbralibrary.media.listener.SoundBoxListener;
import br.com.furbralibrary.ra.VuforiaException;
import br.com.furbralibrary.utils.render.Texture;
import br.com.furbralibrary.utils.render.Dimension;
import br.com.palavras.model.Player;
import br.com.palavras.comm.AllSetToStartMessage;
import br.com.palavras.comm.ResultNotificationMessage;
import br.com.palavras.mesh.CubeObject;
import br.com.palavras.mesh.TexturedObject;

public class MultiGameActivity extends FurbRaTextRecognitionActivity {

    private static final int APLAUSE_SOUND = 0;
    private static final int BEE_SOUND = 1;
    private static final int DOG_SOUND = 2;
    private static final int BUNNY_SOUND = 3;
    private static final int COCK_SOUND = 4;
    private static final int CAT_SOUND = 5;
    private static final int MONKEY_SOUND = 6;
    private static final int MOUSE_SOUND = 7;
    private static final int COW_SOUND = 8;
    private static final int ZEBRA_SOUND = 9;

    private List<String> expectedWords;
    private String currentWord;
    private Dimension dimension;

    /**
     * Se a palavra ainda esta sendo buscada,
     * ou se esta no periodo entre achar a palavra
     * e decidir uma nova.
     */
    private boolean searching = false;

    private Player player = null;
    private boolean currentPlayerTurn;

    private short currentUserScore = 0;
    private short otherUserScore = 0;

    private CubeObject topBarObject;
    private TexturedObject aguardeRep;
    private TexturedObject animalRep;
    private TexturedObject animalLibraRep;
    private TexturedObject correctRep;
    private TexturedObject endNotificationRep;
    private Map<String, Texture> textures;
    private Map<String, TexturedObject> animalsRep;
    private Map<String, TexturedObject> messagesRep;
    private Map<String, TexturedObject> libraRep;
    private float[] projection;
    private boolean renderRight = false;
    private boolean finished = false;
    private Dialog progressDialog = null;
    private boolean soundDone = false;
    private boolean vuforiaDone = false;
    private boolean otherPlayerDone = false;
    private Timer timer;
    private boolean started = false;
    private boolean animalChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Configuração da tela para ocultação da barras de status
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        //Define que somente as palavras da lista adicional devem ser consideradas
        setOnlyWordsInList(true);

        //Recupera a referência do jogador que será o oponente
        recoverPLayer();

        //Carrega os sons dos animais
        loadSounds();

        //Carrega as texturas dos animais
        loadTextures();

        //Define o listener de touch pra disparar a repodução do som
        setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                playAnimalSound();
                return true;
            }
        });

        //Cria a barra de progresso
        createProgressDialog();

        //Registra os listeners de mensagem
        registerCommListeners();
    }

    private void recoverPLayer() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.player = (Player) extras.getSerializable("PLAYER");
            this.currentPlayerTurn = extras.getBoolean("FIRST");
        }
    }

    private void createProgressDialog() {
        progressDialog = new Dialog(this, android.R.style.Theme_Light);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.loading_multi);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void loadTextures() {
        textures = new HashMap<>();
        textures.put("COELHO", Texture.loadTextureFromDrawable(R.drawable.coelho, this));
        textures.put("COELHO_LIBRA", Texture.loadTextureFromDrawable(R.drawable.coelho_libra, this));
        textures.put("CACHORRO", Texture.loadTextureFromDrawable(R.drawable.cachorro, this));
        textures.put("CACHORRO_LIBRA", Texture.loadTextureFromDrawable(R.drawable.cachorro_libra, this));
        textures.put("GALO", Texture.loadTextureFromDrawable(R.drawable.galo, this));
        textures.put("GALO_LIBRA", Texture.loadTextureFromDrawable(R.drawable.galo_libra, this));
        textures.put("GATO", Texture.loadTextureFromDrawable(R.drawable.gato, this));
        textures.put("GATO_LIBRA", Texture.loadTextureFromDrawable(R.drawable.gato_libra, this));
        textures.put("MACACO", Texture.loadTextureFromDrawable(R.drawable.macaco, this));
        textures.put("MACACO_LIBRA", Texture.loadTextureFromDrawable(R.drawable.macaco_libra, this));
        textures.put("RATO", Texture.loadTextureFromDrawable(R.drawable.rato, this));
        textures.put("RATO_LIBRA", Texture.loadTextureFromDrawable(R.drawable.rato_libra, this));
        textures.put("VACA", Texture.loadTextureFromDrawable(R.drawable.vaca, this));
        textures.put("VACA_LIBRA", Texture.loadTextureFromDrawable(R.drawable.vaca_libra, this));
        textures.put("ZEBRA", Texture.loadTextureFromDrawable(R.drawable.zebra, this));
        textures.put("ZEBRA_LIBRA", Texture.loadTextureFromDrawable(R.drawable.zebra_libra, this));
        textures.put("GANHOU", Texture.loadTextureFromDrawable(R.drawable.parabens, this));
        textures.put("PERDEU", Texture.loadTextureFromDrawable(R.drawable.perdeu, this));
        textures.put("EMPATE", Texture.loadTextureFromDrawable(R.drawable.empate, this));
        textures.put("AGUARDE", Texture.loadTextureFromDrawable(R.drawable.aguarde, this));
        textures.put("ACERTOU_LIBRA", Texture.loadTextureFromDrawable(R.drawable.acertou_libra, this));
    }

    private void loadSounds() {
        //Load Sounds
        SoundBox soundBox = SoundBox.getInstance();
        soundBox.addSound(APLAUSE_SOUND, R.raw.applause);
        soundBox.addSound(BUNNY_SOUND, R.raw.coelho);
        soundBox.addSound(COCK_SOUND, R.raw.galo);
        soundBox.addSound(CAT_SOUND, R.raw.gato);
        soundBox.addSound(DOG_SOUND, R.raw.cachorro);
        soundBox.addSound(MONKEY_SOUND, R.raw.macaco);
        soundBox.addSound(MOUSE_SOUND, R.raw.rato);
        soundBox.addSound(COW_SOUND, R.raw.vaca);
        soundBox.addSound(ZEBRA_SOUND, R.raw.zebra);
        soundBox.load(this);

        soundBox.setListener(new SoundBoxListener() {
            @Override
            public void resourceLoadingCompleted() {
                soundDone = true;
                checkForGameStart();
            }
        });
    }

    @Override
    public void onInitARDone(VuforiaException exception) {
        super.onInitARDone(exception);

        this.expectedWords = getWordsInList();
        this.searching = true;
        this.vuforiaDone = true;

        checkForGameStart();
    }

    private void checkForGameStart() {
        if (started) {
            return;
        }
        if (this.vuforiaDone && this.soundDone) {
            try {
                LocalNetworkCommunication.getInstance().sendMessage(this.player.getLanService(), new AllSetToStartMessage());
            } catch (IOException e) {
                new RuntimeException(e);
            }
            if (this.otherPlayerDone) {
                this.started = true;
                progressDialog.dismiss();
            }
        }
    }

    private void nextWord() {
        Log.i(LOGTAG, "Pr�xima palavra chamado: Palavras ainda existentes: ".concat(String.valueOf(expectedWords.size())));
        if (expectedWords.size() > 0) {
            currentWord = this.expectedWords.remove(expectedWords.size() == 1 ? 0 : new Random().nextInt(expectedWords.size() - 1));
            Log.i(LOGTAG, "Pr�xima palavra obtida: ".concat(currentWord));
            this.animalChanged = true;

            final String currentWord = this.currentWord;
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        currentPlayerTurn = false;
                        LocalNetworkCommunication.getInstance().sendMessage(player.getLanService(), new ResultNotificationMessage(false, currentWord));
                        if (expectedWords.isEmpty()) {
                            finishGame();
                        }
                    } catch (IOException e) {
                        new RuntimeException(e);
                    }
                }
            }, 2 * 60 * 1000); //Dois minutos para montar a palavra
        } else {
            finishGame();
        }
    }

    private void finishGame() {
        finished = true;

        if (this.currentUserScore > this.otherUserScore) {
            //Ganhou
            endNotificationRep = messagesRep.get("GANHOU");
        } else if (this.currentUserScore < this.otherUserScore) {
            //Perdeu
            endNotificationRep = messagesRep.get("PERDEU");
        } else {
            //Empate
            endNotificationRep = messagesRep.get("EMPATE");
        }
        endNotificationRep.scale(0.5f, 0.5f, 0);
    }

    private void loadAnimalRep() {
        this.animalsRep = new HashMap<>();
        this.libraRep = new HashMap<>();
        for (String word : expectedWords) {
            TexturedObject texturedObject = new TexturedObject(textures.get(word.toUpperCase()));
            this.animalsRep.put(word.toUpperCase(), texturedObject);

            texturedObject = new TexturedObject(textures.get(word.concat("_libra").toUpperCase()));
            this.libraRep.put(word.toUpperCase(), texturedObject);
        }
    }

    private void loadMessagesRep() {
        this.messagesRep = new HashMap<>();
        //Ganhou
        this.messagesRep.put("GANHOU", new TexturedObject(textures.get("GANHOU")));
        //Perdeu
        this.messagesRep.put("PERDEU", new TexturedObject(textures.get("PERDEU")));
        //Empate
        this.messagesRep.put("EMPATE", new TexturedObject(textures.get("EMPATE")));
    }

    private void prepareAnimalRep() {
        if (this.animalChanged) {
            Log.i(LOGTAG, "Preparando animal ".concat(currentWord.toUpperCase()));
            animalRep = this.animalsRep.get(currentWord.toUpperCase());
            animalRep.scale(0.2f, 0.2f, 1f);
            animalRep.translate(renderRight ? 5f : -5f, 0f, 0f);

            animalLibraRep = new TexturedObject(textures.get(currentWord.concat("_libra").toUpperCase()));
            animalLibraRep.scale(0.2f, 0.2f, 1f);
            animalLibraRep.translate(renderRight ? -5f : 5f, 0f, 0f);

            renderRight = !renderRight;
            this.animalChanged = false;
        }
    }

    @Override
    public void prepareRendering(int width, int height) {
        // Define clear color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
                : 1.0f);

        GLES20.glViewport(0, 0, width, height);

        projection = new float[16];
        // Landscape
        final float aspectRatio = (float) width / (float) height;
        Matrix.orthoM(projection, 0, -aspectRatio, aspectRatio, -1f, 1f, -1f, 1f);

        //Cria barra de resultado
        this.topBarObject = new CubeObject();
        this.topBarObject.setColor(0f, 0f, 0f, 1f);
        this.topBarObject.translate(0f, 0.75f, 0f);
        this.topBarObject.scale(aspectRatio, 0.1f, 1f);

        this.aguardeRep = new TexturedObject(this.textures.get("AGUARDE"));

        this.correctRep = new TexturedObject(this.textures.get("ACERTOU_LIBRA"));
        this.correctRep.scale(0.2f, 0.2f, 1f);

        loadAnimalRep();
        loadMessagesRep();

        if (currentPlayerTurn) {
            nextWord();
        }
    }

    @Override
    public void draw() {
        if (!started) {
            return;
        }

        //Prepara o objeto com a textura do animal
        prepareAnimalRep();

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        if (finished){
            //Renderiza a mensagem de fim
            endNotificationRep.draw(projection);
        }else if (!this.currentPlayerTurn) {
            //Renderiza a mensagem de aguarde
            this.aguardeRep.draw(projection);
        } else if (searching) {
            //Renderiza o animal
            this.animalRep.draw(projection);
            this.animalLibraRep.draw(projection);
        } else {
            //Renderiza a barra de status
            this.topBarObject.draw(projection);
            this.correctRep.draw(projection);
        }

        GLES20.glDisable(GLES20.GL_BLEND);
    }

    @Override
    public void trackerFound(TrackableResult trackableResult, float[] matrix) {
        if (!trackableResult.isOfType(WordResult.getClassType()) || !this.searching)
            return;

        WordResult wordResult = (WordResult) trackableResult;
        Word word = (Word) wordResult.getTrackable();

        if (word.getStringU().equalsIgnoreCase(this.currentWord)) {
            wordFinded();
        }
    }

    private void wordFinded() {
        this.currentWord = "%%%%%%%";
        //Inativa a busca
        this.searching = false;
        //cancela o timer da rodada
        timer.cancel();
        //Atualiza o score
        currentUserScore++;
        //Mostra a barra de status como verde.
        topBarObject.setColor(0, 1f, 0, 1f);
        //Toca o som de aplausos
        SoundBox.getInstance().play(APLAUSE_SOUND);

        //Deixa a mensagem de parabens por 2 segundos
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                searching = true;

                //Oculta a barra de status.
                topBarObject.setColor(0, 1f, 0, 0f);

                try {
                    //Atualiza a flag da rodada
                    currentPlayerTurn = false;
                    //Envia a mensagem quanto ao fim da rodada
                    LocalNetworkCommunication.getInstance().sendMessage(player.getLanService(), new ResultNotificationMessage(true, currentWord));
                    if (expectedWords.size() == 0) {
                        //Caso nao haja mais palavras finaliza o jogo
                        finishGame();
                    }
                } catch (IOException e) {
                    new RuntimeException(e);
                }
            }
        }, 2000);
    }

    private void playAnimalSound() {
        SoundBox soundBox = SoundBox.getInstance();
        switch (this.currentWord.toUpperCase()) {
            case "ABELHA":
                soundBox.play(BEE_SOUND);
                break;
            case "CACHORRO":
                soundBox.play(DOG_SOUND);
                break;
            case "COELHO":
                soundBox.play(BUNNY_SOUND);
                break;
            case "GALO":
                soundBox.play(COCK_SOUND);
                break;
            case "GATO":
                soundBox.play(CAT_SOUND);
                break;
            case "MACACO":
                soundBox.play(MONKEY_SOUND);
                break;
            case "RATO":
                soundBox.play(MOUSE_SOUND);
                break;
            case "VACA":
                soundBox.play(COW_SOUND);
                break;
            case "ZEBRA":
                soundBox.play(ZEBRA_SOUND);
                break;
        }
    }

    @Override
    public void postDraw() {
    }

    @Override
    public Dimension getRoi() {
        if (dimension == null) {
            DisplayMetrics displayMetrics = getDisplayMetrics();

            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;

            dimension = new Dimension();

            dimension.setX(0);
            dimension.setY(0);
            dimension.setHeight(height);
            dimension.setWidth(width);
        }

        return dimension;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        SoundBox.getInstance().unload();
    }

    private void registerCommListeners() {
        final LocalNetworkCommunication instance = LocalNetworkCommunication.getInstance();

        instance.registerLocalNetworkMessageReceivedListener(AllSetToStartMessage.class, new LocalNetworkMessageReceivedListener() {
            public void messageReceived(LanService service, Message message) {
                otherPlayerDone = true;
                checkForGameStart();
            }
        });

        instance.registerLocalExternalServiceDiscoveryListener(new LocalExternalServiceDiscoveryListener() {
            @Override
            public void serviceDiscovered(LanService service) {
            }

            @Override
            public void serviceLost(LanService service) {
                if (service.equals(player.getLanService())) {
                    //Se o servico caiu nao pode se continuar o jogo;
                    showToast("O amiguinho foi desconectado.");
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 5000);
                }
            }
        });

        instance.registerLocalNetworkMessageReceivedListener(ResultNotificationMessage.class, new LocalNetworkMessageReceivedListener() {
            @Override
            public void messageReceived(LanService service, Message message) {
                ResultNotificationMessage msg = (ResultNotificationMessage) message;
                String palavra = msg.getPalavra();
                boolean acertou = msg.isAcertou();
                if (acertou) {
                    otherUserScore++;
                }
                Log.d(LOGTAG, "Outro usuario "+(acertou ? "Acertou" : "Errou")+ " a palavra "+palavra);
                expectedWords.remove(palavra.toLowerCase());

                currentPlayerTurn = true;
                nextWord();
            }
        });
    }

}
