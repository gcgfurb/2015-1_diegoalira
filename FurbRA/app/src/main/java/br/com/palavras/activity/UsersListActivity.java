package br.com.palavras.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.furbra.R;
import br.com.furbralibrary.communication.LanService;
import br.com.furbralibrary.communication.LocalNetworkCommunication;
import br.com.furbralibrary.communication.Message;
import br.com.furbralibrary.communication.listener.LocalExternalServiceDiscoveryListener;
import br.com.furbralibrary.communication.listener.LocalNetworkMessageReceivedListener;
import br.com.palavras.model.Player;
import br.com.palavras.view.adapter.UsersAdapter;
import br.com.palavras.comm.UserGameInviteMessage;
import br.com.palavras.comm.UserGameInviteReplyMessage;
import br.com.palavras.comm.UserNameSolicitationMessage;
import br.com.palavras.comm.UserNameSolicitationReplyMessage;

public class UsersListActivity extends Activity {

    private static final String TAG = "UsersListActivity";

    private UsersAdapter usersAdapter;
    private ProgressDialog waitingAnswer;
    private Map<LanService, String> nameServiceMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_users_list);

        this.nameServiceMap = new HashMap<>();

        prepareListView();

        prepareNSD();
    }

    private void prepareNSD() {
        final LocalNetworkCommunication localNetworkCommunication = LocalNetworkCommunication.getInstance();

        localNetworkCommunication.registerLocalExternalServiceDiscoveryListener(new LocalExternalServiceDiscoveryListener() {
            @Override
            public void serviceDiscovered(LanService service) {
                updateKnowServices();
            }

            @Override
            public void serviceLost(LanService service) {
                updateKnowServices();
            }
        });

        localNetworkCommunication.registerLocalNetworkMessageReceivedListener(UserGameInviteMessage.class, new LocalNetworkMessageReceivedListener() {
            @Override
            public void messageReceived(final LanService service, Message message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isFinishing()) {
                            return;
                        }
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(UsersListActivity.this);
                        alertBuilder.setMessage(String.format(getResources().getString(R.string.msg_convite_recebido), nameServiceMap.get(service)));
                        alertBuilder.setPositiveButton(R.string.opcao_sim, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    LocalNetworkCommunication.getInstance().sendMessage(service, new UserGameInviteReplyMessage(UserGameInviteReplyMessage.YES));
                                    createGame(service, true);
                                } catch (IOException e) {
                                }
                            }
                        });
                        alertBuilder.setNegativeButton(R.string.opcao_nao, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    LocalNetworkCommunication.getInstance().sendMessage(service, new UserGameInviteReplyMessage(UserGameInviteReplyMessage.NO));
                                } catch (IOException e) {
                                }
                            }
                        });
                        alertBuilder.setCancelable(false);
                        alertBuilder.create().show();
                    }
                });
            }
        });

        localNetworkCommunication.registerLocalNetworkMessageReceivedListener(UserGameInviteReplyMessage.class, new LocalNetworkMessageReceivedListener() {
            @Override
            public void messageReceived(LanService service, Message message) {
                //Se esta aguardando uma retorno no convite feito
                if (((UserGameInviteReplyMessage) message).getReply() == UserGameInviteReplyMessage.YES) {
                    if (waitingAnswer != null) {
                        waitingAnswer.dismiss();
                    }
                    createGame(service, false);
                } else {
                    if (waitingAnswer != null) {
                        waitingAnswer.dismiss();
                    }
                }
            }
        });

        localNetworkCommunication.registerLocalNetworkMessageReceivedListener(UserNameSolicitationMessage.class, new LocalNetworkMessageReceivedListener() {
            @Override
            public void messageReceived(LanService service, Message message) {
                try {
                    localNetworkCommunication.sendMessage(service, new UserNameSolicitationReplyMessage(BluetoothAdapter.getDefaultAdapter().getName()));
                } catch (Exception e) {
                    Log.e(TAG, "Falha no envio de mensagem UserNameSolicitationReplyMessage", e);
                }
            }
        });

        localNetworkCommunication.registerLocalNetworkMessageReceivedListener(UserNameSolicitationReplyMessage.class, new LocalNetworkMessageReceivedListener() {
            @Override
            public void messageReceived(LanService service, Message message) {
                nameServiceMap.put(service, ((UserNameSolicitationReplyMessage) message).getName());
                notifyChangeInList();
            }
        });

        localNetworkCommunication.startService(this);
    }

    private void createGame(LanService service, boolean first) {
        LocalNetworkCommunication.getInstance().unregisterAllLocalExternalServiceDiscoveryListener();
        LocalNetworkCommunication.getInstance().unregisterAllLocalNetworkMessageReceivedListener();

        Intent intent = new Intent(UsersListActivity.this, MultiGameActivity.class);
        intent.putExtra("PLAYER", new Player(service, nameServiceMap.get(service)));
        intent.putExtra("FIRST", first);
        startActivityForResult(intent, 0);
    }

    private void updateKnowServices() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nameServiceMap.clear();
                LocalNetworkCommunication localNetworkCommunication = LocalNetworkCommunication.getInstance();
                ArrayList<LanService> knownServices = new ArrayList(localNetworkCommunication.getKnownServices());
                for (LanService service : knownServices) {
                    try {
                        localNetworkCommunication.sendMessage(service, new UserNameSolicitationMessage());
                    } catch (IOException e) {
                        Log.e(TAG, "Falha no envio de mensagem UserNameSolicitationMessage", e);
                    }
                }
                notifyChangeInList();
            }
        });
    }

    private void notifyChangeInList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                usersAdapter.notifyDataSetChanged();
            }
        });
    }

    private void prepareListView() {
        final ListView listView = (ListView) findViewById(R.id.usersList);
        usersAdapter = new UsersAdapter(this, nameServiceMap);
        listView.setAdapter(usersAdapter);
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LanService user = (LanService) listView.getItemAtPosition(position);
                try {
                    LocalNetworkCommunication.getInstance().sendMessage(user, new UserGameInviteMessage());
                    waitingAnswer = ProgressDialog.show(UsersListActivity.this, "", "Aguardando a resposta do amiguinho", true, false);
                } catch (IOException e) {
                    Log.e(TAG, "Falha no envio da mensagem de UserGameInviteMessage");
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        LocalNetworkCommunication.getInstance().startDiscovery();
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalNetworkCommunication.getInstance().stopDiscovery();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalNetworkCommunication.getInstance().tearDown();
    }

    public void atualizarLista(View view) {
        updateKnowServices();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Retornar a tela principal finalizando o nsd
        finish();
    }
}
