package br.com.furbralibrary.communication;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Created by Diego on 18/04/2015.
 */
public class LanService implements Serializable {

    private String serviceName;
    private InetAddress address;

    LanService(String serviceName, InetAddress address) {
        this.serviceName = serviceName;
        this.address = address;
    }

    public String getServiceName() {
        return serviceName;
    }

    public InetAddress getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LanService)) return false;

        LanService that = (LanService) o;

        return !(getAddress() != null ? !getAddress().equals(that.getAddress()) : that.getAddress() != null);

    }

    @Override
    public int hashCode() {
        return getAddress() != null ? getAddress().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "LanService{" +
                "serviceName='" + serviceName + '\'' +
                ", address=" + address +
                '}';
    }
}
