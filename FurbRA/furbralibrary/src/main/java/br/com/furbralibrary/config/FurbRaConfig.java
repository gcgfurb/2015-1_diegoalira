package br.com.furbralibrary.config;

import android.content.Context;
import android.content.res.XmlResourceParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import br.com.furbralibrary.config.exception.FurbRaConfigException;

/**
 * Created by Diego on 19/04/2015.
 */
public class FurbRaConfig {

    //CONSTANTES
    private static final String SERVICE_NAME_KEY = ".furbra-config.network.service-name";
    private static final String SERVICE_PORT_NAME_KEY = ".furbra-config.network.port";
    private static final String VWL_FILE_ASSET_KEY = ".furbra-config.text-recognition.vwl-file-asset";
    private static final String LST_FILE_ASSET_KEY = ".furbra-config.text-recognition.lst-file-asset";
    private static final String VUFORIA_LICENSE_KEY = ".furbra-config.vuforia.license";

    private static FurbRaConfig instance;
    private static final String FURB_RA_CONFIG_FILENAME = "furbra_config";
    private static final String FURB_RA_CONFIG_FILETYPE = "xml";

    private Map<String, String> values;

    private FurbRaConfig(Context context) {
        int identifier = context.getResources().getIdentifier(FURB_RA_CONFIG_FILENAME, FURB_RA_CONFIG_FILETYPE, context.getPackageName());

        //Se não foi achado o arquivo de configuração
        if (identifier == 0) {
            String msg = "Arquivo de configurações não encontrado. "+
                         "O mesmo deve ser inserido na pasta res/xml com o nome furbra_config.xml";
            throw new FurbRaConfigException(FurbRaConfigException.CONFIGURATION_FILE_NOT_FOUND, msg);
        }

        try {
            XmlResourceParser xml = context.getResources().getXml(identifier);
            values = readXML(xml);
        } catch (XmlPullParserException | IOException e) {
            String msg = "Problemas na leitura do Arquivo de configurações";
            throw new FurbRaConfigException(FurbRaConfigException.PROBLEM_PARSING_FILE, msg, e);
        }
    }

    public static FurbRaConfig getInstance(Context context) {
        if (instance == null) {
            instance = new FurbRaConfig(context);
        }
        return instance;
    }

    private Map<String, String> readXML(XmlResourceParser xpp) throws XmlPullParserException, IOException {
        xpp.next();

        Map<String, String> retorno = new HashMap<>();

        String currentLevel = "";

        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_DOCUMENT) {
            } else if (eventType == XmlPullParser.START_TAG) {
                currentLevel = currentLevel.concat("." + xpp.getName());
            } else if (eventType == XmlPullParser.END_TAG) {
                currentLevel = currentLevel.replace("." + xpp.getName(), "");
            } else if (eventType == XmlPullParser.TEXT) {
                retorno.put(currentLevel, xpp.getText().trim());
            }
            eventType = xpp.next();
        }
        return retorno;
    }

    public String getNetworkServiceName() {
        return this.values.get(SERVICE_NAME_KEY);
    }

    public Integer getNetworkServicePort() {
        String sPort = this.values.get(SERVICE_PORT_NAME_KEY);
        return sPort != null ? Integer.valueOf(sPort) : null;
    }

    public String getVwlFileAsset() {
        return this.values.get(VWL_FILE_ASSET_KEY);
    }

    public String getLstFileAsset() {
        return this.values.get(LST_FILE_ASSET_KEY);
    }

    public String getVuforiaLicenseKey() {
        return this.values.get(VUFORIA_LICENSE_KEY);
    }
}