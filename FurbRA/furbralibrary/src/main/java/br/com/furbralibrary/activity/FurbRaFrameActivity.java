package br.com.furbralibrary.activity;

import android.util.Log;

import com.qualcomm.vuforia.Marker;
import com.qualcomm.vuforia.MarkerTracker;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.Tracker;
import com.qualcomm.vuforia.TrackerManager;
import com.qualcomm.vuforia.Vec2F;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import br.com.furbralibrary.ra.VuforiaException;

/**
 * Created by Diego on 28/03/2015.
 */
public abstract class FurbRaFrameActivity extends FurbRaBaseActivity {

    private Set<FrameInfo> newMarkers;
    private Map<Integer, Marker> dataSet;
    private boolean trackersStarted = false;

    protected FurbRaFrameActivity() {
        this.newMarkers = new HashSet<>();
    }

    protected void addMarker(int id, String name, Vec2F size) {
        this.newMarkers.add(new FrameInfo(id, name, size));
        if (this.trackersStarted) {
            doLoadTrackersData();
        }
    }

    protected void removeMarker(int id) {
        //Se o marcador ja foi informadao ao tracker
        if (dataSet.containsKey(id)) {
            TrackerManager tManager = TrackerManager.getInstance();
            MarkerTracker markerTracker = (MarkerTracker) tManager
                    .getTracker(MarkerTracker.getClassType());
            if (markerTracker != null) {
                markerTracker.destroyMarker(dataSet.get(id));
            }
        }else{
            //Se o marcador ainda nao foi informado ao tracker, basta remove-lo da classe
            this.newMarkers.remove(new FrameInfo(id, null, null));
        }
    }

    @Override
    public boolean doInitTrackers() {
        // Indica que os marcadores foram carregados corretamente.
        boolean result = true;

        // Inicializa o tracker especializado no marcador frame
        TrackerManager trackerManager = TrackerManager.getInstance();
        Tracker trackerBase = trackerManager.initTracker(MarkerTracker
                .getClassType());
        MarkerTracker markerTracker = (MarkerTracker) (trackerBase);

        if (markerTracker == null) {
            String errorMessage = "Tracker nao inicializado. Tracker ja iniciado ou a camera ja foi iniciada.";
            Log.e(LOGTAG,errorMessage);
            result = false;
        }

        return result;
    }

    @Override
    public boolean doLoadTrackersData() {
        //Obter o tracker do marcador frame iniciado
        TrackerManager tManager = TrackerManager.getInstance();
        MarkerTracker markerTracker = (MarkerTracker) tManager
                .getTracker(MarkerTracker.getClassType());
        if (markerTracker == null)
            return false;

        dataSet = new LinkedHashMap<>();

        //Criar os marcadores a partir da lista de novos marcadores
        for (FrameInfo newMarker : newMarkers) {
            Marker frameMarker = markerTracker.createFrameMarker(newMarker.getId(), newMarker.getName(), newMarker.getSize());

            if (frameMarker == null) {
                String errorMessage = "Falha na criacao do marcador frame";
                Log.e(LOGTAG, errorMessage);
                throw new VuforiaException(VuforiaException.TRACKERS_INITIALIZATION_FAILURE, errorMessage);
            }

            this.dataSet.put(frameMarker.getId(), frameMarker);
        }

        //Limpar a lista de marcadores.
        newMarkers.clear();

        return true;
    }

    @Override
    public boolean doStartTrackers() {
        TrackerManager tManager = TrackerManager.getInstance();
        MarkerTracker markerTracker = (MarkerTracker) tManager
                .getTracker(MarkerTracker.getClassType());
        if (markerTracker == null) {
            this.trackersStarted = false;
        }else{
            markerTracker.start();
            this.trackersStarted = true;
        }
        return trackersStarted;
    }

    @Override
    public boolean doStopTrackers() {
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        MarkerTracker markerTracker = (MarkerTracker) tManager
                .getTracker(MarkerTracker.getClassType());
        if (markerTracker == null) {
            result = false;
        }else{
            markerTracker.stop();
            this.trackersStarted = false;
        }

        return result;
    }

    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        MarkerTracker markerTracker = (MarkerTracker) tManager
                .getTracker(MarkerTracker.getClassType());
        if (markerTracker != null) {
            for (Marker marker : dataSet.values()) {
                markerTracker.destroyMarker(marker);
            }
        }

        return result;
    }

    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(MarkerTracker.getClassType());

        return result;
    }

    private class FrameInfo {
        private Integer id;
        private String name;
        private Vec2F size;

        private FrameInfo(Integer id, String name, Vec2F size) {
            this.id = id;
            this.name = name;
            this.size = size;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public Vec2F getSize() {
            return size;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof FrameInfo)) {
                return false;
            }

            FrameInfo other = (FrameInfo) o;

            return this.getId().equals(other.getId());
        }
    }

    public void onARUpdate(State state) {}
}