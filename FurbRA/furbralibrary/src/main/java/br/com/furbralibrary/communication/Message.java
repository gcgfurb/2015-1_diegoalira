package br.com.furbralibrary.communication;

import java.io.Serializable;

/**
 * Created by Diego on 18/04/2015.
 */
public abstract class Message implements Serializable {

    private String originaryLanServiceName;

    public String getOriginaryLanServiceName() {
        return originaryLanServiceName;
    }

    void setOriginaryLanServiceName(String originaryLanServiceName) {
        this.originaryLanServiceName = originaryLanServiceName;
    }
}
