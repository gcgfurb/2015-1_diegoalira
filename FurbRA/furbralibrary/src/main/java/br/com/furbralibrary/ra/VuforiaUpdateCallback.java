package br.com.furbralibrary.ra;

import android.content.res.Configuration;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;

import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.Renderer;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Vec2I;
import com.qualcomm.vuforia.VideoBackgroundConfig;
import com.qualcomm.vuforia.VideoMode;
import com.qualcomm.vuforia.Vuforia;
import com.qualcomm.vuforia.Vuforia.UpdateCallbackInterface;

import br.com.furbralibrary.activity.FurbRaBaseActivity;
import br.com.furbralibrary.config.FurbRaConfig;

public class VuforiaUpdateCallback implements UpdateCallbackInterface {

    private static final String LOGTAG = "VuforiaSessionUtil";

    //Activity atual
    private FurbRaBaseActivity activity;

    // Flags
    private boolean started = false;
    private boolean cameraRunning = false;

    // Tamanho de tela do dispositivo
    private int sreenWidth = 0;
    private int screenHeight = 0;

    // Tarefas assincronas para a inicialização do Vuforia
    private InitVuforiaTask initVuforiaTask;
    private LoadTrackerTask loadTrackerTask;

    //Objeto usado para o sincronismo da inicialização e finalização da biblioteca.
    private Object shutdownLock = new Object();

    // Flags de inicialização do Vuforia
    private int vuforiaFlags = Vuforia.GL_20;

    // Dispositivo atual
    private int cameraDevice = CameraDevice.CAMERA.CAMERA_DEFAULT;

    // Orientação atual
    private boolean isPortrait = false;

    /**
     * Inicializa o Vuforia
     *
     * @param activity Activity atual.
     */
    public void initAR(FurbRaBaseActivity activity) {
        VuforiaException vuforiaException = null;
        this.activity = activity;

        //Armazenamento da orientação do dispositivo
        updateActivityOrientation();

        //Armazenamento das dimensões da tela do dispositivo
        storeScreenDimensions();

        //Inicialização assincrona do dispositivo de modo a impedir o bloqueio da thread principal (UI)
        if (initVuforiaTask != null) {
            String logMessage = "Não pode inicializar o SDK Vuforia mais de uma vez.";
            vuforiaException = new VuforiaException(VuforiaException.VUFORIA_ALREADY_INITIALIZATED,
                    logMessage);
            Log.e(LOGTAG, logMessage);
        }else{
            try {
                (initVuforiaTask = new InitVuforiaTask()).execute();
            } catch (Exception e) {
                String logMessage = "Inicialização do SDK Vuforia falhou.";
                vuforiaException = new VuforiaException(VuforiaException.INITIALIZATION_FAILURE,
                        logMessage);
                Log.e(LOGTAG, logMessage);
            }
        }
        if (vuforiaException != null)
            this.activity.onInitARDone(vuforiaException);
    }

    /**
     * Iniciar o Vuforia, inicializando a camera e os marcadores.
     *
     * @throws VuforiaException Exceção de inicialização do SDK Vuforia.
     */
    public void startAR() throws VuforiaException {
        String error;
        if (cameraRunning) {
            error = "Camera em funcionamento, não é possivel utilizar.";
            Log.e(LOGTAG, error);
            throw new VuforiaException(
                    VuforiaException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        //Obtem e inicializa o recurso
        if (!CameraDevice.getInstance().init(cameraDevice)) {
            error = "Não foi possivel obter o recurso de câmera: " + cameraDevice;
            Log.e(LOGTAG, error);
            throw new VuforiaException(
                    VuforiaException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        configureVideoBackground();

        if (!CameraDevice.getInstance().selectVideoMode(
                CameraDevice.MODE.MODE_DEFAULT)) {
            error = "Não foi possivel definir o modo de camera";
            Log.e(LOGTAG, error);
            throw new VuforiaException(
                    VuforiaException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        //Inicia a câmera
        if (!CameraDevice.getInstance().start()) {
            error = "Não foi possivel inicializar a camera do dispositivo: " + cameraDevice;
            Log.e(LOGTAG, error);
            throw new VuforiaException(
                    VuforiaException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        //Inicia os marcadores
        activity.doStartTrackers();

        cameraRunning = true;

        if (!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO)) {
            CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
        }
    }


    /**
     * Para o SDK Vuforia, parando qualquer inicialização pendente.
     *
     * @throws VuforiaException Exceção de inicialização do SDK Vuforia.
     */
    public void stopAR() throws VuforiaException {
        //Finaliza asyncTasks de inicialização que podem estar em execução.
        if (initVuforiaTask != null
                && initVuforiaTask.getStatus() != InitVuforiaTask.Status.FINISHED) {
            initVuforiaTask.cancel(true);
            initVuforiaTask = null;
        }
        if (loadTrackerTask != null
                && loadTrackerTask.getStatus() != LoadTrackerTask.Status.FINISHED) {
            loadTrackerTask.cancel(true);
            loadTrackerTask = null;
        }

        initVuforiaTask = null;
        loadTrackerTask = null;

        started = false;

        stopCamera();

         /**
         * Obtem lock do objeto de sincronia de modo a executar a finalizaçao do SDK sincronamente
         * de maneira  a assegurar que a rotina não se sobrepõe ao carregamento dos marcadores
          * ou a inicialização da biblioteca.
         */
        synchronized (shutdownLock) {

            boolean unloadTrackersResult;
            boolean deinitTrackersResult;

            //Destrói os dados de marcadores já carregados.
            unloadTrackersResult = activity.doUnloadTrackersData();

            //Finaliza os gerenciadores de marcadores
            deinitTrackersResult = activity.doDeinitTrackers();

            // Finaliza o SDK Vuforia
            Vuforia.deinit();

            if (!unloadTrackersResult)
                throw new VuforiaException(
                        VuforiaException.UNLOADING_TRACKERS_FAILURE,
                        "Não foi possivel destuir os dados dos marcadores carregados.");

            if (!deinitTrackersResult)
                throw new VuforiaException(
                        VuforiaException.TRACKERS_DEINITIALIZATION_FAILURE,
                        "Não foi possivel finalizar os gerenciadores de marcadores");
        }
    }

    /**
     * Reinicializa o SDK Vuforia, reiniciando os marcadores e a camera.
     * @throws VuforiaException Exceção de inicialização do SDK Vuforia.
     */
    public void resumeAR() throws VuforiaException {
        Vuforia.onResume();

        if (started) {
            startAR();
        }
    }

    /**
     * Pausa o SDK Vuforia e a camera.
     */
    public void pauseAR() {
        if (started) {
            stopCamera();
        }

        Vuforia.onPause();
    }

    /**Callback do SDK executado a cada ciclo.
     *
     * @param s Objeto estado do AR, possuindo em si o frame atual analisado pelo SDK e os marcadores encontrados.
     */
    @Override
    public void QCAR_onUpdate(State s) {
        activity.onARUpdate(s);
    }

    /**
     * Gerencia as mudanças na configuração do dispositivo.
     */
    public void onConfigurationChanged() {
        updateActivityOrientation();

        storeScreenDimensions();

        if (isARRunning()) {
            configureVideoBackground();
        }
    }

    // Returns the error message for each error code
    private String getInitializationErrorString(int code) {
        if (code == Vuforia.INIT_DEVICE_NOT_SUPPORTED)
            return "Falha na inicialização do Vuforia. Dispositivo não suportado.";
        if (code == Vuforia.INIT_NO_CAMERA_ACCESS)
            return "Falha na inicialização do Vuforia. A câmera não está acessivel.";
        if (code == Vuforia.INIT_LICENSE_ERROR_MISSING_KEY)
            return "Chave Vuforia do aplicativo não informada.";
        if (code == Vuforia.INIT_LICENSE_ERROR_INVALID_KEY)
            return "Chave inválida utilizada.";
        if (code == Vuforia.INIT_LICENSE_ERROR_NO_NETWORK_TRANSIENT)
            return "Não foi possivel contatar os servidores da QualComm.";
        if (code == Vuforia.INIT_LICENSE_ERROR_NO_NETWORK_PERMANENT)
            return "Sem rede disponivel. Por favor se conecte a uma rede com acesso a internet.";
        if (code == Vuforia.INIT_LICENSE_ERROR_CANCELED_KEY)
            return "Esta licença foi cancelada e não pode mais ser utilizada.";
        else {
            return "Falha na inicialização do Vuforia.";
        }
    }

    /**
     * Armazena as dimensões da tela.
     */
    private void storeScreenDimensions() {
        // Query display dimensions:
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        sreenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
    }

    /**
     * Armazena a orientação do dispositivo.
     */
    private void updateActivityOrientation() {
        Configuration config = activity.getResources().getConfiguration();

        switch (config.orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                isPortrait = true;
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                isPortrait = false;
                break;
            case Configuration.ORIENTATION_UNDEFINED:
            default:
                break;
        }
    }

    /**
     * Para o dispositivo de video.
     */
    public void stopCamera() {
        if (cameraRunning) {
            activity.doStopTrackers();
            CameraDevice.getInstance().stop();
            CameraDevice.getInstance().deinit();
            cameraRunning = false;
        }
    }

    /**
     * Configura o modo de video e as proporções a imagem da camera.
     */
    private void configureVideoBackground() {
        CameraDevice cameraDevice = CameraDevice.getInstance();
        VideoMode vm = cameraDevice.getVideoMode(CameraDevice.MODE.MODE_DEFAULT);

        VideoBackgroundConfig config = new VideoBackgroundConfig();
        config.setEnabled(true);
        config.setSynchronous(true);
        config.setPosition(new Vec2I(0, 0));

        int xSize = 0, ySize = 0;
        if (isPortrait) {
            xSize = (int) (vm.getHeight() * (screenHeight / (float) vm
                    .getWidth()));
            ySize = screenHeight;

            if (xSize < sreenWidth) {
                xSize = sreenWidth;
                ySize = (int) (sreenWidth * (vm.getWidth() / (float) vm
                        .getHeight()));
            }
        } else {
            xSize = sreenWidth;
            ySize = (int) (vm.getHeight() * (sreenWidth / (float) vm
                    .getWidth()));

            if (ySize < screenHeight) {
                xSize = (int) (screenHeight * (vm.getWidth() / (float) vm
                        .getHeight()));
                ySize = screenHeight;
            }
        }

        config.setSize(new Vec2I(xSize, ySize));

        Log.i(LOGTAG, "Background Video : Video (" + vm.getWidth()
                + " , " + vm.getHeight() + "), Tela (" + sreenWidth + " , "
                + screenHeight + "), Tamanho (" + xSize + " , " + ySize + ")");

        Renderer.getInstance().setVideoBackgroundConfig(config);

    }

    /**
     * Obtem se o Vuforia e os marcadores já foram inicializados.
     * @return Se o Vuforia e os marcadores já foram inicializados.
     */
    public boolean isARRunning() {
        return started;
    }

    // AsyncTask para iniciar o Vuforia assincronamente
    private class InitVuforiaTask extends AsyncTask<Void, Integer, Boolean> {
        private int progressValue = -1;

        protected Boolean doInBackground(Void... params) {
            // Bloco sincronizado para previnir sobreposição dos métodos de inicialização e finalização:
            synchronized (shutdownLock) {
                //Informa a bliblioteca a Activity que ira rodar a RA, as flags de inicialização e a licença do aplicativo
                Vuforia.setInitParameters(activity, vuforiaFlags, FurbRaConfig.getInstance(activity).getVuforiaLicenseKey());

                do {
                    //Vuforia.init() bloqueia a execução até um progresso na execução ter ocorrido
                    //retornando valores de 0 a 100, que representam a conclusão da inicialização.
                    //Este método retorna -1 ou menor no caso de erros, sendo que este
                    //valor negativo representa o motivo da falha.
                    //A finalização acaba quando chegar a 100%.
                    progressValue = Vuforia.init();

                    // Sai do loop nos seguintes casos:
                    //a) O processo foi cancelado
                    //b) O processo chegou a 100%
                    //c) Ocorreu um erro na inicialização da biblioteca.
                } while (!isCancelled() && progressValue >= 0
                        && progressValue < 100);

                return (progressValue > 0);
            }
        }

        protected void onPostExecute(Boolean result) {
            VuforiaException vuforiaException = null;

            if (result) {
                //Caso a biblioteca tenha sido inicializada corretamente

                Log.d(LOGTAG, "InitVuforiaTask.onPostExecute: Vuforia iniciado");

                //Chama o método responsável pela configuração inicial dos marcadores na activity
                boolean initTrackersResult = activity.doInitTrackers();

                if (initTrackersResult) {
                    //Caso os marcadores tenham sido configurados conrretamente
                    try {
                        //Carregamento assincrono das informações dos marcadores
                        loadTrackerTask = new LoadTrackerTask();
                        loadTrackerTask.execute();
                    } catch (Exception e) {
                        String logMessage = "Carregamento dos dados dos marcadores falhou";
                        vuforiaException = new VuforiaException(
                                VuforiaException.LOADING_TRACKERS_FAILURE,
                                logMessage);
                        Log.e(LOGTAG, logMessage);
                    }
                } else {
                    String logMessage = "Falha na configuração dos marcadores.";
                    vuforiaException = new VuforiaException(
                            VuforiaException.TRACKERS_INITIALIZATION_FAILURE,
                            logMessage);
                    Log.e(LOGTAG, logMessage);
                }
            } else {
                //Caso a biblioteca não tenha sido inicializada corretamente

                //Obtem-se o motivo especifico da falha no carregamento de maneira a apresentar no log
                String logMessage = getInitializationErrorString(progressValue);
                Log.e(LOGTAG, "InitVuforiaTask.onPostExecute: " + logMessage + " Finalizando.");

                vuforiaException = new VuforiaException(
                        VuforiaException.INITIALIZATION_FAILURE,
                        logMessage);
            }

            if (vuforiaException != null) {
                // Finaliza o processo de inicialização com a exceção representando o erro
                activity.onInitARDone(vuforiaException);
            }
        }
    }

    // Um AsyncTask para o carregamento assincrono dos dados dos marcadores.
    private class LoadTrackerTask extends AsyncTask<Void, Integer, Boolean> {
        protected Boolean doInBackground(Void... params) {
            // Bloco sincronizado para previnir sobreposição dos métodos de inicialização e finalização:
            synchronized (shutdownLock) {
                // Carrega os dados dos marcadores.
                return activity.doLoadTrackersData();
            }
        }

        protected void onPostExecute(Boolean result) {
            VuforiaException vuforiaException = null;

            if (!result) {
                String logMessage = "LoadTrackerTask.onPostExecute: falha no carregamento dos marcadores.";
                Log.e(LOGTAG, logMessage);
                vuforiaException = new VuforiaException(
                        VuforiaException.LOADING_TRACKERS_FAILURE,
                        logMessage);
            } else {
                Log.d(LOGTAG, "LoadTrackerTask.onPostExecute: dados dos marcadores carregados.");
                System.gc();

                //Registra com a biblioteca a classe para ser atualizada a cada ciclo através do método QCAR_onUpdate
                Vuforia.registerCallback(VuforiaUpdateCallback.this);

                //Marca a inicilização como concluida.
                started = true;
            }

            //Finaliza o processo de inicialização da biblioteca;
            activity.onInitARDone(vuforiaException);
        }
    }

}
