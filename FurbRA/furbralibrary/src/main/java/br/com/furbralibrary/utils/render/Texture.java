/*===============================================================================
Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of QUALCOMM Incorporated, registered in the United States 
and other countries. Trademarks of QUALCOMM Incorporated are used with permission.
===============================================================================*/

package br.com.furbralibrary.utils.render;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class Texture {

    private static final String LOGTAG = "Texture";

    private int width;          // Largura da Textura
    private int height;         // Altura da Textura
    private int channels;       // Número de canais
    private ByteBuffer data;    // Pixel data.
    private int[] textureID = new int[1];

    protected Texture() {
    }

    /**
     * Carrega textura a partir de um recurso drawable.
     *
     * @param resourceId Identificador do recurso (normalmente obtido da classe R)
     * @param context    Contexto
     * @return objeto Texture baseado no recurso drawable
     */
    public static Texture loadTextureFromDrawable(int resourceId, Context context) {
        InputStream inputStream = context.getResources().openRawResource(resourceId);
        return loadTextureFromStream(inputStream);
    }


    /**
     * Carrega textura a partir de um arquivo contido na pasta asset.
     *
     * @param fileName Nome do arquivo.
     * @param assets   Gerenciado de assets.
     * @return objeto Texture baseado no recurso drawable
     */
    public static Texture loadTextureFromAsset(String fileName, AssetManager assets) {
        InputStream inputStream = null;
        try {
            inputStream = assets.open(fileName, AssetManager.ACCESS_BUFFER);

            return loadTextureFromStream(inputStream);
        } catch (IOException e) {
            Log.e(LOGTAG, "Falha ao carregar a textura '" + fileName);
            Log.i(LOGTAG, e.getMessage());
            return null;
        }
    }

    private static Texture loadTextureFromStream(InputStream inputStream) {
        BufferedInputStream bufferedStream = new BufferedInputStream(
                inputStream);
        Bitmap bitMap = BitmapFactory.decodeStream(bufferedStream);

        int[] data = new int[bitMap.getWidth() * bitMap.getHeight()];
        bitMap.getPixels(data, 0, bitMap.getWidth(), 0, 0,
                bitMap.getWidth(), bitMap.getHeight());

        return loadTextureFromIntBuffer(data, bitMap.getWidth(),
                bitMap.getHeight());
    }

    private static Texture loadTextureFromIntBuffer(int[] data, int width,
                                                    int height) {
        // Convert:
        int numPixels = width * height;
        byte[] dataBytes = new byte[numPixels * 4];

        for (int p = 0; p < numPixels; ++p) {
            int colour = data[p];
            dataBytes[p * 4] = (byte) (colour >>> 16); // R
            dataBytes[p * 4 + 1] = (byte) (colour >>> 8); // G
            dataBytes[p * 4 + 2] = (byte) colour; // B
            dataBytes[p * 4 + 3] = (byte) (colour >>> 24); // A
        }

        Texture texture = new Texture();
        texture.width = width;
        texture.height = height;
        texture.channels = 4;

        texture.data = ByteBuffer.allocateDirect(dataBytes.length).order(
                ByteOrder.nativeOrder());
        int rowSize = texture.width * texture.channels;
        for (int r = 0; r < texture.height; r++)
            texture.data.put(dataBytes, rowSize * (texture.height - 1 - r),
                    rowSize);

        texture.data.rewind();

        // Cleans variables
        dataBytes = null;
        data = null;

        return texture;
    }

    public void bindTexture() {
        GLES20.glGenTextures(1, textureID, 0);
        OpenGLUtils.checkGLError("glGenTextures texture");

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureID[0]);
        OpenGLUtils.checkGLError("glBindTexture texture");

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        OpenGLUtils.checkGLError("glTexParameterf texture");

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        OpenGLUtils.checkGLError("glTexParameterf texture");

        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                width, height, 0, GLES20.GL_RGBA,
                GLES20.GL_UNSIGNED_BYTE, data);
        OpenGLUtils.checkGLError("glTexImage2D texture");

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getChannels() {
        return channels;
    }

    public ByteBuffer getData() {
        return data;
    }

    public int[] getTextureID() {
        return textureID;
    }
}
