package br.com.furbralibrary.media.exception;

/**
 * Created by Diego on 08/04/2015.
 */
public class SoundBoxLoadedException extends RuntimeException {

    public static final int SOUND_BOX_ALREADY_LOADED = 0;
    public static final int SOUND_BOX_NOT_LOADED = 1;

    private int errorCode;

    public SoundBoxLoadedException(int errorCode, String detailMessage) {
        super(detailMessage);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
