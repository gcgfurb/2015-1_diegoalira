package br.com.furbralibrary.render;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import com.qualcomm.vuforia.Matrix44F;
import com.qualcomm.vuforia.Renderer;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Tool;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.Vuforia;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import br.com.furbralibrary.activity.FurbRaBaseActivity;

/**
 * Created by Diego on 25/03/2015.
 */
public class FurbRaRenderer implements GLSurfaceView.Renderer {

    private final String LOGTAG = this.getClass().getName();

    private final FurbRaBaseActivity activity;
    private boolean active;

    public FurbRaRenderer(FurbRaBaseActivity furbRaBaseActivity) {
        this.activity = furbRaBaseActivity;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d(LOGTAG, "FurbRaRenderer.onSurfaceCreated");

        Vuforia.onSurfaceCreated();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d(LOGTAG, "FurbRaRenderer.onSurfaceChanged");

        Vuforia.onSurfaceChanged(width, height);

        activity.prepareRendering(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        if (!active)
            return;

        renderFrame();
    }

    private void renderFrame() {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        //Marca o inicio da renderizacao
        State state = Renderer.getInstance().begin();

        //Solicitar a biblioteca a renderizacao da imagem da camera no background
        Renderer.getInstance().drawVideoBackground();

        //Repassar a renderizacao a Activity
        activity.draw();

        for (int id = 0; id < state.getNumTrackableResults(); id++){
            //Obtem o marcador detectado
            TrackableResult trackableResult = state.getTrackableResult(id);

            //Obtem a matriz modelView baseado no marcador
            Matrix44F modelViewMatrix_Vuforia = Tool
                    .convertPose2GLMatrix(trackableResult.getPose());
            float[] modelViewMatrix = modelViewMatrix_Vuforia.getData();

            //Repassa o marcador e a matriz a Activity
            activity.trackerFound(trackableResult, modelViewMatrix);
        }

        //Evento de finalizacao da renderizacao
        activity.postDraw();

        //Marca o fim da renderizacao
        Renderer.getInstance().end();
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
