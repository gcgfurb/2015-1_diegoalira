package br.com.furbralibrary.communication.listener;

import br.com.furbralibrary.communication.LanService;

/**
 * Created by Diego on 18/04/2015.
 */
public interface LocalExternalServiceDiscoveryListener {

    void serviceDiscovered(LanService service);

    void serviceLost(LanService service);

}
