package br.com.furbralibrary.utils;

/**
 * Created by Diego on 01/05/2015.
 */
public final class Constants {

    public static final int BYTES_PER_FLOAT = 4;
    public static final int BYTES_PER_SHORT = 2;

}
