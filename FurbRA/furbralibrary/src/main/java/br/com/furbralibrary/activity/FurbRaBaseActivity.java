package br.com.furbralibrary.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.TrackableResult;

import br.com.furbralibrary.R;
import br.com.furbralibrary.ra.VuforiaException;
import br.com.furbralibrary.ra.VuforiaUpdateCallback;
import br.com.furbralibrary.render.FurbRaGLView;

/**
 * Classe base para a criação de Activities baseadas em recursos oferecidos
 * pela biblioteca de RA Vuforia.
 *
 * Created by Diego on 25/03/2015.
 */
public abstract class FurbRaBaseActivity extends Activity {

    protected final String LOGTAG = this.getClass().getName();

    private FurbRaGLView glView;
    protected VuforiaUpdateCallback vuforiaUpdateCallback;
    private RelativeLayout uILayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);

        addOverlayView();

        //Configuração necessária para manter tela do dispositivo ligada durante a activity
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        this.vuforiaUpdateCallback = new VuforiaUpdateCallback();
    }

    @Override
    protected void onResume() {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        if (!this.vuforiaUpdateCallback.isARRunning()) {
            this.vuforiaUpdateCallback.initAR(this);
        }

        vuforiaUpdateCallback.resumeAR();

        // Resume the GL view:
        if (glView != null) {
            glView.onResume();
        }
    }

    public void onInitARDone(VuforiaException exception) {
        if (exception == null) {
            glView = new FurbRaGLView(this);

            addContentView(glView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            prepareOverlayView();

            vuforiaUpdateCallback.startAR();
        } else {
            Log.e(LOGTAG, exception.getString());
            throw exception;
        }
    }

    private void prepareOverlayView() {
        uILayout.bringToFront();
        uILayout.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        vuforiaUpdateCallback.onConfigurationChanged();
    }

    /**
     * Adicionar a camada acima da visualização da camera.
     */
    private void addOverlayView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        uILayout = (RelativeLayout) inflater.inflate(
                R.layout.camera_overlay, null, false);

        uILayout.setVisibility(View.VISIBLE);
        uILayout.setBackgroundColor(Color.BLACK);

        addContentView(uILayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    protected void onPause() {
        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (glView != null) {
            glView.onPause();
        }

        vuforiaUpdateCallback.pauseAR();
    }

    @Override
    protected void onDestroy() {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        vuforiaUpdateCallback.stopAR();
    }

    /**
     * Método útil para a criação de mensagens (Toast) em tela.
     * @param text Mensagem a ser apresentada.
     */
    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Método útil para a obtenção do DisplayMetrics da tela atual.
     * @return {@link DisplayMetrics} da tela atual.
     */
    protected DisplayMetrics getDisplayMetrics() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    /**
     * Obtem se a biblioteca Vuforia terminou de inicializar.
     * @return Se a biblioteca Vuforia terminou de inicializar.
     */
    protected boolean isVuforiaStarted() {
        return this.vuforiaUpdateCallback.isARRunning();
    }

    /**
     * Método para a adição de listener OnTouchListener na tela atual.
     * @param listener Listener que será notificado dos toques em tela.
     */
    public void setOnTouchListener(View.OnTouchListener listener) {
        this.uILayout.setOnTouchListener(listener);
    }

    //Métodos de gerenciamento de marcadores.

    /**
     * Método chamado para se realizar a definição e carga dos rastreadores.
     * @return Se o os rastreadores foram corretamente inicializados.
     */
    public abstract boolean doInitTrackers();

    /**
     * Método chamado para se realizar a carga de dados dos rastreadores.
     * @return Se o os dados dos rastreadores foram corretamente carregados.
     */
    public abstract boolean doLoadTrackersData();

    /**
     * Método chamado para se realizar a inicialização dos rastreadores com os dados carregados.
     * @return Se o os rastreadores foram corretamente inicializados.
     */
    public abstract boolean doStartTrackers();

    /**
     * Método chamado para se realizar uma pausa nos rastreadores.
     * @return Se o os rastreadores foram corretamente pausados.
     */
    public abstract boolean doStopTrackers();

    /**
     * Método chamado para se realizar a destruição dos dados dos rastreadores.
     * @return Se o os dados dos rastreadores foram corretamente destruidos.
     */
    public abstract boolean doUnloadTrackersData();

    /**
     * Método chamado para se realizar a finalização dos rastreadores.
     * @return Se o os dados dos rastreadores foram corretamente finalizados.
     */
    public abstract boolean doDeinitTrackers();

    //Metodos notificadores quanto a RA

    public abstract void trackerFound(TrackableResult trackable, float[] matrix);

    public abstract void onARUpdate(State s);

    //Métodos de renderização.

    public abstract void prepareRendering(int width, int height);

    public abstract void draw();

    public abstract void postDraw();

}