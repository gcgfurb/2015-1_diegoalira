package br.com.furbralibrary.render;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.Log;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

import br.com.furbralibrary.activity.FurbRaBaseActivity;

/**
 * Created by Diego on 25/03/2015.
 */
public class FurbRaGLView extends GLSurfaceView {
    private static final String LOGTAG = "FurbRaGLView";

    private final FurbRaRenderer renderer;

    public FurbRaGLView(FurbRaBaseActivity context) {
        super(context);

        // Determina a versao do OpenGL ES
        setEGLContextClientVersion(2);

        //Solicita a utilizacao de formato que permite transparencia
        this.getHolder().setFormat(PixelFormat.TRANSLUCENT);

        renderer = new FurbRaRenderer(context);
        setRenderer(renderer);
        renderer.setActive(true);
    }

}

