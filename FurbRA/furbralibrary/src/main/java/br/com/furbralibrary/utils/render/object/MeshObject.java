package br.com.furbralibrary.utils.render.object;

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import br.com.furbralibrary.utils.Constants;
import br.com.furbralibrary.utils.render.RenderProgram;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.GL_UNSIGNED_SHORT;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * Created by Diego on 26/04/2015.
 */
public abstract class MeshObject {

    protected float[] modelMatrix;
    protected RenderProgram renderProgram;
    private int positionAttribLocaltion;
    private int matrixUniformLocation;

    protected FloatBuffer bufferVertices;
    protected ShortBuffer bufferIndices;

    private int indicesSize = 0;

    public MeshObject(float[] vertices, short[] indices, RenderProgram renderProgram) {
        this.bufferVertices = createBuffer(vertices);
        this.bufferIndices = createBuffer(indices);
        this.renderProgram = renderProgram;
        this.indicesSize = indices.length;

        modelMatrix = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);

        this.positionAttribLocaltion = glGetAttribLocation(this.renderProgram.getProgramLocation(), getPositionAttribName());
        this.matrixUniformLocation = glGetUniformLocation(this.renderProgram.getProgramLocation(), getPositionMatrixUniformName());
    }

    public void clearTransformations() {
        Matrix.setIdentityM(modelMatrix, 0);
    }

    public void scale(float x, float y, float z) {
        Matrix.scaleM(this.modelMatrix, 0, x, y, z);
    }

    public void translate(float x, float y, float z) {
        Matrix.translateM(modelMatrix, 0, x, y, z);
    }

    public void rotate(float ang, float x, float y, float z) {
        Matrix.rotateM(modelMatrix, 0, ang, x, y, z);
    }

    public void draw(float[] viewProjectionMatrix) {
        glUseProgram(this.renderProgram.getProgramLocation());

        final float[] modelProjection = new float[16];
        Matrix.multiplyMM(modelProjection, 0, viewProjectionMatrix, 0, modelMatrix, 0);

        glUniformMatrix4fv(matrixUniformLocation, 1, false, modelProjection, 0);

        bufferVertices.position(0);
        glVertexAttribPointer(positionAttribLocaltion, getNumPositionCoordsToVertice(), GL_FLOAT, false, 0, bufferVertices);
        glEnableVertexAttribArray(positionAttribLocaltion);

        bufferIndices.position(0);
        glDrawElements(GL_TRIANGLE_FAN, indicesSize, GL_UNSIGNED_SHORT, bufferIndices);
    }

    protected FloatBuffer createBuffer(float[] array) {
        return ByteBuffer.allocateDirect(array.length * Constants.BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer().put(array);
    }

    protected ShortBuffer createBuffer(short[] array) {
        return ByteBuffer.allocateDirect(array.length * Constants.BYTES_PER_SHORT)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer().put(array);
    }

    protected abstract int getNumPositionCoordsToVertice();

    protected abstract String getPositionAttribName();

    protected abstract String getPositionMatrixUniformName();

}
