package br.com.furbralibrary.activity;

import android.util.DisplayMetrics;
import android.util.Log;

import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.RectangleInt;
import com.qualcomm.vuforia.STORAGE_TYPE;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.TextTracker;
import com.qualcomm.vuforia.Tracker;
import com.qualcomm.vuforia.TrackerManager;
import com.qualcomm.vuforia.VideoMode;
import com.qualcomm.vuforia.WordList;

import java.util.ArrayList;
import java.util.List;

import br.com.furbralibrary.config.FurbRaConfig;
import br.com.furbralibrary.utils.render.OpenGLUtils;
import br.com.furbralibrary.utils.render.Dimension;


/**
 * Activity para detecção de textos;
 * <p/>
 * Created by Diego on 31/03/2015.
 */
public abstract class FurbRaTextRecognitionActivity extends FurbRaBaseActivity {

    /**
     * Se deve ser desconsiderado a a lista de palavras contida
     * no arquivo .vwl e ser considerado somente as palavras da
     * lista contida no arquivo .lst passado no método {@link #getWordList()}.
     */
    private boolean onlyWordsInList;

    /**
     * Area de interesse, no qual será observado o texto. </br>
     * ATENÇÂO: QUANTO MAIOR O ROI, MAIS PROCESSAMENTO NECESSÁRIO.
     */
    public abstract Dimension getRoi();

    private String getWordListPath(){
        return FurbRaConfig.getInstance(this).getLstFileAsset();
    }

    private String getVuforiaDictionaryPath(){
        return FurbRaConfig.getInstance(this).getVwlFileAsset();
    }

    @Override
    public boolean doInitTrackers() {
        Log.i(LOGTAG, "doInitTrackers");

        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker = tManager.initTracker(TextTracker.getClassType());
        if (tracker == null) {
            Log.e(LOGTAG,
                    "Tracker não inicializado. Tracker já iniciado ou a câmera já foi iniciada.");
            result = false;
        }

        return result;
    }

    @Override
    public boolean doLoadTrackersData() {
        Log.i(LOGTAG, "doLoadTrackersData");

        boolean loaded = true;

        String vuforiaDictionaryPath = getVuforiaDictionaryPath();
        Log.d(LOGTAG, vuforiaDictionaryPath);

        WordList wl = getWordList();
        if (wl != null && vuforiaDictionaryPath != null && !vuforiaDictionaryPath.isEmpty()) {
            //Carrega as palavras do dicionário vwl
            loaded = wl.loadWordList(vuforiaDictionaryPath,
                    STORAGE_TYPE.STORAGE_APPRESOURCE);

            if ((getWordListPath() != null && !getWordListPath().isEmpty())) {
                //Carrega as palavras adicionais do arquivo lst
                wl.addWordsFromFile(getWordListPath(),
                        STORAGE_TYPE.STORAGE_APPRESOURCE);

                if (isOnlyWordsInList()) {
                    //Caso deseja-se desconsiderar as palavras do dicionário e
                    // rastrear somente as palavars existentes no arquivo adicional lst
                    wl.setFilterMode(WordList.FILTER_MODE.FILTER_MODE_WHITE_LIST);
                    wl.loadFilterList(getWordListPath(),
                            STORAGE_TYPE.STORAGE_APPRESOURCE);
                }
            }
        }else{
            loaded = false;
        }

        return loaded;
    }

    @Override
    public boolean doStartTrackers() {
        Log.i(LOGTAG, "doStartTrackers");

        boolean result = true;

        Tracker textTracker = TrackerManager.getInstance().getTracker(
                TextTracker.getClassType());
        if (textTracker != null) {
            textTracker.start();

            configureROI();
        }

        return result;
    }

    @Override
    public boolean doStopTrackers() {
        Log.i(LOGTAG, "doStopTrackers");

        boolean result = true;

        Tracker textTracker = TrackerManager.getInstance().getTracker(
                TextTracker.getClassType());
        if (textTracker != null)
            textTracker.stop();

        return result;
    }

    @Override
    public boolean doUnloadTrackersData() {
        Log.i(LOGTAG, "doUnloadTrackersData");

        boolean result = true;

        WordList wl = getWordList();
        if (wl != null) {
            wl.unloadAllLists();
        }
        return result;
    }

    @Override
    public boolean doDeinitTrackers() {
        Log.i(LOGTAG, "doDeinitTrackers");

        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(TextTracker.getClassType());

        return result;
    }

    private WordList getWordList() {
        TrackerManager tm = TrackerManager.getInstance();
        TextTracker tt = (TextTracker) tm
                .getTracker(TextTracker.getClassType());
        if (tt == null) {
            return null;
        }
        return tt.getWordList();
    }

    public List<String> getWordsInList() {
        List<String> list = new ArrayList<>();

        WordList wordList = getWordList();

        for (int i = 0; i < wordList.getFilterListWordCount() ;i++) {
            list.add(wordList.getFilterListWordU(i));
        }

        return list;
    }

    public void configureROI() {
        VideoMode vm = CameraDevice.getInstance().getVideoMode(
                CameraDevice.MODE.MODE_DEFAULT);

        DisplayMetrics metrics = getDisplayMetrics();
        int screenWidth = metrics.widthPixels;
        int screenHeight = metrics.heightPixels;

        int[] loupeCenterX = {0};
        int[] loupeCenterY = {0};
        int[] loupeWidth = {0};
        int[] loupeHeight = {0};

        // Converte as coordenadas da tela em coordenadas de câmera
        OpenGLUtils.screenCoordToCameraCoord(getRoi().getCenterX(),
                getRoi().getCenterY(), getRoi().getWidth(),
                getRoi().getHeight(), screenWidth, screenHeight,
                vm.getWidth(), vm.getHeight(), loupeCenterX, loupeCenterY,
                loupeWidth, loupeHeight);

        RectangleInt detROI = new RectangleInt(loupeCenterX[0]
                - (loupeWidth[0] / 2), loupeCenterY[0] - (loupeHeight[0] / 2),
                loupeCenterX[0] + (loupeWidth[0] / 2), loupeCenterY[0]
                + (loupeHeight[0] / 2));

        TextTracker tt = (TextTracker) TrackerManager.getInstance().getTracker(
                TextTracker.getClassType());
        if (tt != null) {
            tt.setRegionOfInterest(detROI, detROI, TextTracker.UP_DIRECTION.REGIONOFINTEREST_UP_IS_0_HRS);
        }
    }

    public void onARUpdate(State state) {}

    public boolean isOnlyWordsInList() {
        return onlyWordsInList;
    }

    public void setOnlyWordsInList(boolean onlyWordsInList) {
        this.onlyWordsInList = onlyWordsInList;
    }
}
