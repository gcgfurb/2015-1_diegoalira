package br.com.furbralibrary.utils.render;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Diego on 01/05/2015.
 */
public class RenderProgram {

    private static final String LOGTAG = "RenderProgram";

    private int programLocation;
    private int vertexShaderLocation;
    private int fragmentShaderLocation;

    public RenderProgram(Context context, int vertexShaderResource, int fragmentShaderResource) throws IOException {
        //TODO: Declarar no DOC que deve ser um arquivo glsl (verificar se esse é o formato) que deve existir no raw
        String vertexShader = loadShaderFromResource(context, vertexShaderResource);
        String fragmentShader = loadShaderFromResource(context, fragmentShaderResource);

        createProgram(vertexShader, fragmentShader);
    }

    public RenderProgram(String vertexShader, String fragmentShader) {
        createProgram(vertexShader, fragmentShader);
    }

    private int initShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        if (shader == 0) {
            //TODO: Lançar exception de impossibilidade de criação de espaço em memória.
        }else{
            GLES20.glShaderSource(shader, source);
            GLES20.glCompileShader(shader);

            int[] glStatusVar = {GLES20.GL_FALSE};
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, glStatusVar,
                    0);
            if (glStatusVar[0] == GLES20.GL_FALSE) {
                Log.e(LOGTAG, "Não foi possivel compilar o shader " + shaderType + " : "
                        + GLES20.glGetShaderInfoLog(shader));
                GLES20.glDeleteShader(shader);
                shader = 0;
            }
        }
        return shader;
    }

    private void createProgram(String vertexShader,
                             String fragmentShader) {
        this.vertexShaderLocation = initShader(GLES20.GL_VERTEX_SHADER, vertexShader);
        this.fragmentShaderLocation = initShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);

        int program = GLES20.glCreateProgram();
        if (program != 0) {
            GLES20.glAttachShader(program, vertexShaderLocation);
            OpenGLUtils.checkGLError("glAttchShader(vert)");

            GLES20.glAttachShader(program, fragmentShaderLocation);
            OpenGLUtils.checkGLError("glAttchShader(frag)");

            GLES20.glLinkProgram(program);
            int[] glStatusVar = {GLES20.GL_FALSE};
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, glStatusVar,
                    0);
            if (glStatusVar[0] == GLES20.GL_FALSE) {
                Log.e(
                        LOGTAG,
                        "Não foi possivel carregar o programa : "
                                + GLES20.glGetProgramInfoLog(program));

                GLES20.glDeleteProgram(program);
            }
        }

        this.programLocation = program;
    }

    private String loadShaderFromResource(Context context, int resourceId) throws IOException {
        StringBuilder glsl = new StringBuilder();

        InputStream inputStream = context.getResources().openRawResource(resourceId);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String nextLine;
        while ((nextLine = bufferedReader.readLine()) != null) {
            glsl.append(nextLine);
        }

        return glsl.toString();
    }

    public int getProgramLocation() {
        return programLocation;
    }

}
