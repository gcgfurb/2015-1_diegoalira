package br.com.furbralibrary.communication.listener;

import br.com.furbralibrary.communication.LanService;
import br.com.furbralibrary.communication.Message;

/**
 * Created by Diego on 18/04/2015.
 */
public interface LocalNetworkMessageReceivedListener {

    void messageReceived(LanService service, Message message);

}
