package br.com.furbralibrary.communication;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.furbralibrary.communication.exception.LocalNetworkCommunicationStartServiceException;
import br.com.furbralibrary.communication.listener.LocalExternalServiceDiscoveryListener;
import br.com.furbralibrary.communication.listener.LocalNetworkMessageReceivedListener;
import br.com.furbralibrary.config.FurbRaConfig;

/**
 * Created by Diego on 16/03/2015.
 */
public class LocalNetworkCommunication {

    private static final String TAG = "LocalNetworkCommunication";
    private static final String SERVICE_TYPE = "_http._tcp.";

    private static LocalNetworkCommunication instance;
    private NsdManager nsdManager = null;
    private Set<LanService> knownServices = null;

    //Listeners do componente
    private List<LocalExternalServiceDiscoveryListener> localExternalServiceDiscoveryListeners;
    private Map<Class<? extends Message>, List<LocalNetworkMessageReceivedListener>> localNetworkMessageReceivedListeners;

    //NSD Listeners
    private NsdManager.RegistrationListener registrationListener;
    private NsdManager.DiscoveryListener discoveryListener;

    /**
     * Pode ser alterado de forma automática pelo {@link #initializeRegistrationListener()}
     * no momento do registro do serviço.
     */
    private String currentServiceName;

    /**
     * Nome original do serviço.
     */
    private String defaultServiceName;

    /**
     * Thread de recebimento de comunicação.
     */
    private Thread serverThread;

    /**
     * ServerSocket de recebimento de comunicações.
     */
    private ServerSocket serverSocket;

    /**
     * Endereço local formatado
     */
    private String localAddress;

    /**
     * Porta local.
     */
    private int port;

    /**
     * Flag quanto ao estado do serviço.
     */
    private boolean serviceActive = false;

    /**
     * Flag quanto ao estado de descoberta de serviços de rede.
     */
    private boolean discoveryActive = false;

    private LocalNetworkCommunication() {
        this.knownServices = new HashSet<>();
        this.localAddress = "";
    }

    public void startService(Context context) {
        if (this.serviceActive) {
            return;
        }
        this.currentServiceName = "";

        //Obtenção das configurações
        FurbRaConfig furbRaConfig = FurbRaConfig.getInstance(context);
        this.defaultServiceName = furbRaConfig.getNetworkServiceName();
        this.port = furbRaConfig.getNetworkServicePort();

        //Validação
        if (this.defaultServiceName == null || this.defaultServiceName.isEmpty()) {
            throw new LocalNetworkCommunicationStartServiceException(LocalNetworkCommunicationStartServiceException.SERVICE_NAME_NOT_INFORMED, "Nome do serviço de comunicação não informado no arquivo de configurações.");
        }else if (this.port == 0) {
            throw new LocalNetworkCommunicationStartServiceException(LocalNetworkCommunicationStartServiceException.SERVICE_PORT_NOT_INFORMED, "Porta do serviço de comunicação não informado no arquivo de configurações.");
        }

        //Criação da informação do serviço
        NsdServiceInfo serviceInfo = prepareServiceInfo();

        //Registro do ip local
        registerLocalAddress(context);

        //Inicia a thread de recebimento de mensagens
        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();

        //Intancia o listener do registro
        initializeRegistrationListener();

        //Registra o serviço
        this.nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        this.nsdManager.registerService(serviceInfo,
                NsdManager.PROTOCOL_DNS_SD, this.registrationListener);
    }

    //Necessita da <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    public void registerLocalAddress(Context context) {
        WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        this.localAddress = Formatter.formatIpAddress(ip);
        Log.d(TAG, "Ip local registrado "+localAddress);
    }

    public static final int DISCOVERY_SOLICITED = 0;
    public static final int DISCOVERY_FAILURE_SERVICE_NOT_STARTED = 1;
    public static final int DISCOVERY_FAILURE_ALREADY_STARTED = 2;

    public int startDiscovery() {
        if (!this.serviceActive) {
            return DISCOVERY_FAILURE_SERVICE_NOT_STARTED;
        }
        if (this.discoveryActive) {
            return DISCOVERY_FAILURE_ALREADY_STARTED;
        }

        initializeDiscoveryListener();

        nsdManager.discoverServices(
                SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);

        return DISCOVERY_SOLICITED;
    }

    private NsdServiceInfo prepareServiceInfo() {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            Log.e(TAG, "Não foi possivel criar o socket para o servidor NSD");
            throw new RuntimeException(e);
        }

        Log.i(TAG, "SocketServer criado. Porta: " + port);

        NsdServiceInfo serviceInfo = new NsdServiceInfo();
        serviceInfo.setServiceName(defaultServiceName);
        serviceInfo.setServiceType(SERVICE_TYPE);
        serviceInfo.setPort(port);
        return serviceInfo;
    }

    public void stopService() {
        if (!this.serviceActive) {
            return;
        }

        nsdManager.unregisterService(registrationListener);

        if (this.serverThread != null) {
            this.serverThread.interrupt();
            this.serverThread = null;
        }
        if (this.serverSocket != null && !this.serverSocket.isClosed()) {
            try {
                this.serverSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Erro ao fechar ServerSocket");
            }
        }
        this.serverSocket = null;
    }

    public void stopDiscovery() {
        if (!this.discoveryActive) {
            return;
        }

        nsdManager.stopServiceDiscovery(discoveryListener);
    }

    public void tearDown() {
        stopDiscovery();
        stopService();
        this.knownServices.clear();
    }

    public static LocalNetworkCommunication getInstance() {
        if (instance == null) {
            instance = new LocalNetworkCommunication();
        }
        return instance;
    }

    private void initializeRegistrationListener() {
        this.registrationListener = new NsdManager.RegistrationListener() {

            @Override
            public void onServiceRegistered(NsdServiceInfo NsdServiceInfo) {
                /*
                 * Embora o nome do serviço seja definido no registro,
                 * ele deve ser único na rede, desta forma pode ser alterado pela aplicação
                 * no registro.
                 */
                currentServiceName = NsdServiceInfo.getServiceName();
                serviceActive = true;
                Log.d(TAG, "Inicio do serviço NSD com o nome: " + currentServiceName);

                startDiscovery();
            }

            @Override
            public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                String error = getNSDInicializationErrorMessage(errorCode);
                Log.e(TAG, "Erro no registro do Serviço NSD: " + error + "; ErrorCode: " + errorCode);
                registrationListener = null;
                serviceActive = false;
            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo arg0) {
                Log.d(TAG, "Finalização do serviço NSD");

                /* A referencia ao listener somente é removida após a a finalização do desregstro do serviço.
                 * Isso é feito pelo fato que o NSDManager mantem referencias fracas dos listeners.
                 * Se a referência fosse limpa antes, quando o NSDManager fosse utilizar o listener, o mesmo
                 * corria o risco de ja ter sio coletado pelo GarbageCollector, resultando numa referência nula.
                 */
                registrationListener = null;
                serviceActive = false;

            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                String error = getNSDInicializationErrorMessage(errorCode);
                Log.e(TAG, "Erro na remoção do registro do Serviço NSD: " + error + "; ErrorCode: " + errorCode);
                registrationListener = null;
                serviceActive = false;
            }
        };
    }

    private void initializeDiscoveryListener() {
        this.discoveryListener = new NsdManager.DiscoveryListener() {

            @Override
            public void onDiscoveryStarted(String regType) {
                Log.i(TAG, "Serviço de descoberta NSD iniciado");
                discoveryActive = true;
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                String serviceName = fixServiceName(service.getServiceName());
                if (serviceName.equals(currentServiceName)) {
                    //Se for o serviço local
                } else if (service.getServiceType().equals(SERVICE_TYPE)
                        && (serviceName.contains(defaultServiceName) ||
                        serviceName.equals(defaultServiceName))) {
                    Log.i(TAG, "Serviço encontrado: " + service);
                    nsdManager.resolveService(service, new NsdResolveListener());
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                Log.i(TAG, "NSD: Serviço remoto perdido" + service);
                LanService lanService = findServiceByAddress(service.getHost());
                removeServiceFromKnowList(lanService);
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "NSD: Serviço de descoberta de rede finalizado: " + serviceType);
                discoveryActive = false;
                discoveryListener = null;
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                String error = getNSDInicializationErrorMessage(errorCode);
                Log.e(TAG, "NSD: Inicio do serviço de descoberta falhou: " + error + "; Error code:" + errorCode);
                discoveryActive = false;
                discoveryListener = null;
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                String error = getNSDInicializationErrorMessage(errorCode);
                Log.e(TAG, "NSD: Finalização do serviço de descoberta falhou: " + error + "; Error code:" + errorCode);
                discoveryActive = false;
                discoveryListener = null;
            }
        };
    }

    private void removeServiceFromKnowList(LanService lanService) {
        if (knownServices.remove(lanService)) {
            fireLocalExternalServiceLost(lanService);
        }
    }

    private LanService findServiceByName(String serviceName) {
        String fixServiceName = fixServiceName(serviceName);
        for (LanService service : knownServices) {
            if (service.getServiceName().equals(fixServiceName)) {
                return service;
            }
        }
        return null;
    }

    private LanService findServiceByAddress(InetAddress host) {
        for (LanService service : knownServices) {
            if (service.getAddress().equals(host)) {
                return service;
            }
        }
        return null;
    }

    private String getNSDInicializationErrorMessage(int errorCode) {
        String error = "";
        switch (errorCode) {
            case NsdManager.FAILURE_INTERNAL_ERROR:
                error = "Erro interno";
                break;
            case NsdManager.FAILURE_ALREADY_ACTIVE:
                error = "Serviço já esta ativo";
                break;
            case NsdManager.FAILURE_MAX_LIMIT:
                error = "Máximo número de pedidos foram atingidos";
                break;
        }
        return error;
    }

    public void sendMessage(LanService lanService, Message message) throws IOException {
        if (this.currentServiceName == null) {
            throw new IllegalStateException("É necessário iniciar o serviço NSD para enviar mensagens.");
        }
        if (lanService == null) {
            throw new IllegalArgumentException("O serviço destino não pode ser nulo.");
        }
        if (message == null) {
            throw new IllegalArgumentException("Não é possivel enviar uma mensagem nula.");
        }
        Log.i(TAG, "Enviando mensagem do tipo :" + message.getClass().getCanonicalName() + " para " + fixServiceName(lanService.getServiceName()));
        //Setar a origem na mensagem
        message.setOriginaryLanServiceName(this.currentServiceName);
        new Thread(new OutcomeCommunicationThread(lanService, message)).start();
    }

    //LocalNetworkMessageReceivedListener methods
    public void registerLocalNetworkMessageReceivedListener(Class<? extends Message> messageType, LocalNetworkMessageReceivedListener listener) {
        if (this.localNetworkMessageReceivedListeners == null) {
            this.localNetworkMessageReceivedListeners = new HashMap<>();
        }

        List<LocalNetworkMessageReceivedListener> listenersOfType = this.localNetworkMessageReceivedListeners.get(messageType);
        if (listenersOfType == null) {
            listenersOfType = new ArrayList<>();
            this.localNetworkMessageReceivedListeners.put(messageType, listenersOfType);
        }

        listenersOfType.add(listener);
    }

    public void unregisterLocalNetworkMessageReceivedListener(Class<? extends Message> messageType, LocalNetworkMessageReceivedListener listener) {
        if (this.localNetworkMessageReceivedListeners == null) {
            return;
        }

        List<LocalNetworkMessageReceivedListener> listenersOfType = this.localNetworkMessageReceivedListeners.get(messageType);
        if (listenersOfType == null) {
            return;
        }

        listenersOfType.remove(listener);
    }

    public void unregisterAllLocalNetworkMessageReceivedListener() {
        this.localNetworkMessageReceivedListeners = new HashMap<>();
    }

    public List<LocalNetworkMessageReceivedListener> getRegisteredLocalNetworkMessageReceivedListenerForMessageType(Class<? extends Message> messageType) {
        if (this.localNetworkMessageReceivedListeners == null) {
            return Collections.emptyList();
        }

        List<LocalNetworkMessageReceivedListener> listenersOfType = this.localNetworkMessageReceivedListeners.get(messageType);
        if (listenersOfType == null) {
            return Collections.emptyList();
        }

        return Collections.unmodifiableList(listenersOfType);
    }

    private void fireMessageReceivedListeners(LanService service, Message message) {
        if (message == null) {
            return;
        }

        List<LocalNetworkMessageReceivedListener> listeners = getRegisteredLocalNetworkMessageReceivedListenerForMessageType(message.getClass());
        for (LocalNetworkMessageReceivedListener listener : listeners) {
            listener.messageReceived(service, message);
        }
    }

    public void unregisterAllLocalExternalServiceDiscoveryListener() {
        this.localExternalServiceDiscoveryListeners = new ArrayList<>();
    }

    //LocalExternalServiceDiscoveryListener methods
    public void registerLocalExternalServiceDiscoveryListener(LocalExternalServiceDiscoveryListener listener) {
        if (this.localExternalServiceDiscoveryListeners == null) {
            this.localExternalServiceDiscoveryListeners = new ArrayList<>();
        }
        this.localExternalServiceDiscoveryListeners.add(listener);
    }

    public void unregisterLocalExternalServiceDiscoveryListener(LocalExternalServiceDiscoveryListener listener) {
        if (this.localExternalServiceDiscoveryListeners == null) {
            return;
        }
        this.localExternalServiceDiscoveryListeners.remove(listener);
    }

    public List<LocalExternalServiceDiscoveryListener> getRegisteredLocalExternalServiceDiscoveryListener() {
        if (this.localExternalServiceDiscoveryListeners == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(localExternalServiceDiscoveryListeners);
    }

    public void fireLocalExternalServiceDiscovered(LanService lanService) {
        if (this.localExternalServiceDiscoveryListeners == null) {
            return;
        }
        for (LocalExternalServiceDiscoveryListener listener : this.localExternalServiceDiscoveryListeners) {
            listener.serviceDiscovered(lanService);
        }
    }

    public void fireLocalExternalServiceLost(LanService lanService) {
        if (this.localExternalServiceDiscoveryListeners == null) {
            return;
        }
        for (LocalExternalServiceDiscoveryListener listener : this.localExternalServiceDiscoveryListeners) {
            listener.serviceLost(lanService);
        }
    }

    public Collection<LanService> getKnownServices() {
        return Collections.unmodifiableCollection(knownServices);
    }

    /**
     * Thread server cujo endereço foi anunciado no registro do serviço.
     * Responsável por receber assincronamente as comunicações.
     */
    private class ServerThread implements Runnable {
        public void run() {
            Socket socket;
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    socket = serverSocket.accept();

                    Log.d(TAG, localNetworkMessageReceivedListeners == null ? "IncomeMessage: Listeners nulos" : "IncomeMessage: Listeners QTD: " + localNetworkMessageReceivedListeners.size());

                    //Se não existe listener registrado não existe motivo de continuar o processo.
                    if (localNetworkMessageReceivedListeners != null) {
                        IncomeCommunicationThread commThread = new IncomeCommunicationThread(socket);
                        new Thread(commThread).start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Thread criada a cada recebimento de comunicação da thread server.
     * Responsável por reveber a comunicação, parsear e notificar os interessados.
     */
    private class IncomeCommunicationThread implements Runnable {

        private InetAddress inetAddress;
        private ObjectInputStream objectInputStream;

        public IncomeCommunicationThread(Socket socket) {
            try {
                inetAddress = socket.getInetAddress();
                objectInputStream = new ObjectInputStream(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            try {
                Message message = (Message) objectInputStream.readObject();
                objectInputStream.close();
                LanService knowService = findServiceByAddress(inetAddress);
                if (knowService == null) {
                    //Se o serviço não é conhecido acrescenta-se esse a lista
                    knowService = includeServiceKnowList(message.getOriginaryLanServiceName(), inetAddress);
                }
                Log.d(TAG, "Mensagem recebida do tipo: " + message.getClass() + " de " + message.getOriginaryLanServiceName());
                fireMessageReceivedListeners(knowService, message);
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Algumas versões do android podem apresentar problemas no nome do serviço NSD que possuem espaços,
     * trocando esses por \\032. Este método ajusta essa situação.
     * //https://code.google.com/p/android/issues/detail?id=55913
     *
     * @param serviceName
     * @return
     */
    private String fixServiceName(String serviceName) {
        return serviceName == null ? "" : serviceName.replace("\\\\032", " ");
    }

    private LanService includeServiceKnowList(String serviceName, InetAddress host) {
        if (this.currentServiceName.equals(serviceName) ||
                localAddress.equals(host.getHostAddress())) {
            //Se for esse próprio dispositivo, ignora
            return null;
        }

        LanService lanService = new LanService(fixServiceName(serviceName), host);
        knownServices.add(lanService);
        Log.i(TAG, "Serviço incluido: " + lanService);
        fireLocalExternalServiceDiscovered(lanService);
        return lanService;
    }

    private class OutcomeCommunicationThread implements Runnable {

        private final LanService lanService;
        private Message message;

        public OutcomeCommunicationThread(LanService lanService, Message message) throws IOException {
            this.message = message;
            this.lanService = lanService;
        }

        public void run() {
            try {
                Log.i(TAG, "Mensagem sendo enviada para o endereço: IP:" + lanService.getAddress());
                Socket socket = new Socket(lanService.getAddress(), port);
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                out.writeObject(message);
                out.flush();
                out.close();
            } catch (ConnectException e) {
                Log.e(TAG, "Conexão recusada pelo destinatario: " + fixServiceName(lanService.getServiceName()) + ":" + String.valueOf(lanService.getAddress()), e);
                //Se houve falha de conexão remove-se o serviço das redes conhecidas.
                removeServiceFromKnowList(lanService);
            } catch (IOException e) {
                Log.e(TAG, "Falha na conexão com o serviço: " + fixServiceName(lanService.getServiceName()) + ":" + String.valueOf(lanService.getAddress()), e);
            }
        }
    }

    private class NsdResolveListener implements NsdManager.ResolveListener {

        @Override
        public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
            String error = getNSDInicializationErrorMessage(errorCode);
            Log.e(TAG, "Não foi possivel resolver o serviço NSD. SERVIÇO: " + serviceInfo + " ERRO:" + error + " ERRORCODE:" + errorCode);
            //Se era um serviço conhecido, remove-se da lista
            removeServiceFromKnowList(findServiceByName(serviceInfo.getServiceName()));
        }

        @Override
        public void onServiceResolved(NsdServiceInfo serviceInfo) {
            Log.i(TAG, "Serviço Resolvido. " + serviceInfo);
            includeServiceKnowList(serviceInfo.getServiceName(), serviceInfo.getHost());
        }
    }

}