package br.com.furbralibrary.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.furbralibrary.media.exception.SoundBoxLoadedException;
import br.com.furbralibrary.media.listener.SoundBoxListener;

/**
 * Created by Diego on 06/04/2015.
 */
public class SoundBox {

    private static SoundBox instance;

    /**
     * Map de identificadores do usuários e identificadores dos arquvios de áudio (identificadores obtidos atraves do R.raw.(id))
     */
    private Map<Integer, Integer> soundBoard;
    private SoundPool pool;

    private int soundsLoaded = 0;

    private boolean loaded = false;
    private SoundBoxListener listener = new SoundBoxListener() {
        //Implementação de NullObject
        @Override
        public void resourceLoadingCompleted() {
        }
    };

    private SoundBox() {
        this.soundBoard = new HashMap<Integer, Integer>();
    }

    public static SoundBox getInstance() {
        if (instance != null) {
            return instance;
        }

        instance = new SoundBox();
        return instance;
    }

    private void createPool() {
        this.pool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        initiateLoadingCompleteListener();
    }

    private void initiateLoadingCompleteListener() {
        SoundPool.OnLoadCompleteListener onLoadCompleteListener = new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                if (++soundsLoaded == soundBoard.size()) {
                    loaded = true;
                    listener.resourceLoadingCompleted();
                }
            }
        };
        this.pool.setOnLoadCompleteListener(onLoadCompleteListener);
    }

    public void addSound(int identifier, int resourceId) {
        if (loaded) {
            throw new SoundBoxLoadedException(SoundBoxLoadedException.SOUND_BOX_ALREADY_LOADED, "Não foi possivel adicionar. SoundBox já carregado.");
        }

        soundBoard.put(identifier, resourceId);
    }

    public void load(Context context) {
        if (loaded) {
            throw new SoundBoxLoadedException(SoundBoxLoadedException.SOUND_BOX_ALREADY_LOADED, "SoundBox já carregado");
        }

        createPool();
        new LoadSoundsTask().execute(context);
    }

    public void unload() {
        checkIfLoaded();

        this.soundBoard = new HashMap<>();

        this.pool.release();
        this.pool = null;
        this.soundsLoaded = 0;

        this.loaded = false;
    }

    private void checkIfLoaded() {
        if (!loaded) {
            throw new SoundBoxLoadedException(SoundBoxLoadedException.SOUND_BOX_NOT_LOADED, "SoundBox não foi carregado");
        }
    }

    public boolean isLoaded() {
        return this.loaded;
    }

    public void play(int id) {
        checkIfLoaded();
        this.pool.play(this.soundBoard.get(id), 1.0f, 1.0f, 0, 0, 1);
    }

    public void stop(int id) {
        checkIfLoaded();
        this.pool.stop(this.soundBoard.get(id));
    }

    public void pause(int id) {
        checkIfLoaded();
        this.pool.pause(this.soundBoard.get(id));
    }

    public void resume(int id) {
        checkIfLoaded();
        this.pool.pause(this.soundBoard.get(id));
    }

    public SoundBoxListener getListener() {
        return listener;
    }

    public void setListener(SoundBoxListener listener) {
        this.listener = listener;
    }

    private class LoadSoundsTask extends AsyncTask<Context, Void, Void> {
        protected Void doInBackground(Context... params) {
            Map<Integer, Integer> loadedValues = new HashMap();
            //Mapa de ids de resources e ids de pool
            for (Integer resId : new HashSet<>(soundBoard.values())) {
                if (loadedValues.containsKey(resId)) {
                    continue;
                }
                int loadId = pool.load(params[0], resId, 1);
                loadedValues.put(resId, loadId);
            }

            //Atualiza o id do soundbourd do ID do res pelo ID do SoundPool
            for (Map.Entry<Integer, Integer> entry : soundBoard.entrySet()) {
                entry.setValue(loadedValues.get(entry.getValue()));
            }

            return null;
        }
    }

}