package br.com.furbralibrary.config.exception;

/**
 * Created by Diego on 19/04/2015.
 */
public class FurbRaConfigException extends RuntimeException {

    public static final int CONFIGURATION_FILE_NOT_FOUND = 0;
    public static final int PROBLEM_PARSING_FILE = 1;

    private int errorCode;

    public FurbRaConfigException(int errorCode, String detailMessage) {
        super(detailMessage);
        this.errorCode = errorCode;
    }

    public FurbRaConfigException(int errorCode, String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
