package br.com.furbralibrary.communication.exception;

/**
 * Created by Diego on 24/05/2015.
 */
public class LocalNetworkCommunicationStartServiceException extends RuntimeException {

    public static final int SERVICE_NAME_NOT_INFORMED = 0;
    public static final int SERVICE_PORT_NOT_INFORMED = 1;

    private int erroCode;

    public LocalNetworkCommunicationStartServiceException(int errorCode, String detailMessage) {
        super(detailMessage);
        this.erroCode = errorCode;
    }

    public int getErroCode() {
        return erroCode;
    }
}
