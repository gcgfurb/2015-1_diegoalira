package br.com.furbralibrary.utils.render;

/**
 * Pojo que armazena dimensão e localização.
 *
 * Created by Diego on 02/04/2015.
 */
public class Dimension {

    private int x;
    private int y;
    private int width;
    private int height;

    public Dimension() {}

    public Dimension(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getCenterX() {
        return (x+getWidth()) / 2;
    }

    public int getCenterY() {
        return (y+getHeight()) /2;
    }
}
